import firebase from 'firebase';
 
  

  class FirebaseSDK {  
    // constructor() {
    //   if (!firebase.apps.length) {
    //     //avoid re-initializing
    //     firebase.initializeApp({
    //       apiKey: "AIzaSyD0JL6XP4w5rxsogB2ZYFdfRky-erdmAD8",
    //       authDomain: "chatapp-f57d9.firebaseapp.com",
    //       databaseURL: "https://chatapp-f57d9.firebaseio.com",
    //       projectId: "chatapp-f57d9",
    //       storageBucket: "chatapp-f57d9.appspot.com",
    //       messagingSenderId: "1058435846489",
    //       appId: "1:1058435846489:web:4fb1b24c8376b886"
    //     });
    //   }
    // }

    constructor() {
      if (!firebase.apps.length) {
        //avoid re-initializing
        rrfirebase.initializeApp({
        //   apiKey: "AIzaSyCL8pldLwhaAkR047zLxuW1IydpvP8eHe4",
        //   authDomain: "twmtheapp-9cead.firebaseapp.com",
        //   databaseURL: "https://twmtheapp-9cead.firebaseio.com",
        //   projectId: "twmtheapp-9cead",
        //   storageBucket: "twmtheapp-9cead.appspot.com",
        //   messagingSenderId: "397476397753",
        //   appId: "1:397476397753:android:c61cc839daef7074"
        apiKey: "AIzaSyBSEd9230H9QuZw31yfxytelQ0eym_P5Z4",
        authDomain: "twmtheapp-9cead.firebaseapp.com",
        databaseURL: "https://twmtheapp-9cead.firebaseio.com",
        projectId: "twmtheapp-9cead",
        storageBucket: "twmtheapp-9cead.appspot.com",
        messagingSenderId: "397476397753",
        appId: "1:397476397753:web:bc399f3374652be2"
        });
      }
    }

    login = async (user, success_callback, failed_callback) => {
      await firebase
        .auth()
        .signInWithEmailAndPassword(user.email, user.password)
        .then(success_callback, failed_callback);
    };
  }

  const Firebase = new FirebaseSDK();  


  export default Firebase;