
import React, {Component} from 'react';
import {StyleSheet,Dimensions} from 'react-native';
const deviceWidth = Dimensions.get('screen').width;
const offset = 16;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    input:{
      padding:10,
      borderWidth:1,
      borderColor:'#cccccc',
      width:'70%',
      marginBottom:10,
      borderRadius:5
    },
    btnText:{
      color:'darkblue',
      fontSize:20
    },
    btn:{
      width:deviceWidth-20,
      justifyContent:'center'
    },
    btnContainer:{
        marginTop:offset,
        alignItems:'center',
       justifyContent:'center',
       flexDirection:'row',
       padding:10,
    },
    imgBackground: {
      width: deviceWidth,
     height: '35%',
      flex: 1 ,
      alignItems:'center'
},
startScanBtn: {
  width:deviceWidth-20,
  paddingLeft: 15,
  paddingRight: 15,
  borderRadius: 25,
  alignItems:'center',
},
buttonText: {
  fontSize: 16,
  //fontFamily: 'Gill Sans',
  textAlign: 'center',
  margin: 10,
  color: '#ffffff',
  fontWeight:'bold',
  backgroundColor: 'transparent',
},

   
  });

  export default styles;
  
