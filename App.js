import React, {Component} from 'react';
import {Platform, StyleSheet,AsyncStorage,TouchableOpacity,Alert,TextInput, Text, View,Image,Dimensions, ImageBackgound} from 'react-native';
import { createSwitchNavigator, createStackNavigator, createAppContainer,createBottomTabNavigator, DrawerNavigator, DrawerItems, createDrawerNavigator, Header } from 'react-navigation';
import { Container, Body, Content, Thumbnail } from 'native-base';
import firebase from 'firebase';
import base64 from 'react-native-base64';
import { Notification } from 'react-native-firebase';

import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import ChatScreen from './screens/ChatScreen';
import ProfileScreen from './screens/ProfileScreen';
import Signup from './screens/Signup.';
import CreateGroup from './screens/CreateGroup';
import Groups from './screens/Groups';
import GroupChatSingle from './screens/GroupChatSingle';
import GroupOptions from './screens/GroupOptions';
import AddGroupMembers from './screens/AddGroupMembers';
import GroupMembers from './screens/GroupMembers';
import CountriesScreen from './screens/CountriesScreen';
import CitiesScreen from './screens/CitiesScreen';
import ChatCity from './screens/ChatCity';
import SettingsScreen from './screens/SettingsScreen';
import User from './User';
import firebaseSDK from './firebaseSDK';

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen, 
      navigationOptions: {
          header: null
      }
    },
    Chat: ChatScreen,
  },
  {
    navigationOptions: {
      headerTitle: 'Home',
    }
  }
);

const ExploreStack = createStackNavigator(
  {
    Explore: {
      screen: CountriesScreen, 
      navigationOptions: {
          header: null
      }
    },
    City:         CitiesScreen,
    Chat:         ChatScreen,
    ChatByCity:   ChatCity,
    CreateGroup:  CreateGroup,
    GroupChat:    GroupChatSingle,
    AddMember:    AddGroupMembers,
    GroupMembers: GroupMembers,
    GroupOptions: GroupOptions,
  },
  {
    navigationOptions: {
      headerTitle: 'Explore',
    }
  }
);

const GroupStack = createStackNavigator(
  {
    Groups: {
      screen: Groups, 
      navigationOptions: {
          header: null
      }
    },
    Public:       Groups,
    Private:      Groups,
    CreateGroup:  CreateGroup,
    GroupChat:    GroupChatSingle,
    AddMember:    AddGroupMembers,
    GroupMembers: GroupMembers,
    GroupOptions: GroupOptions,
  },  
  {
    navigationOptions:  {
      headerTitle:'Groups',
    }
  }
);

const ProfileStack = createStackNavigator(
  {
    Profile:  {
      screen: ProfileScreen, 
      navigationOptions: {
          header: null
      }
    },
  },
  {
    navigationOptions:  {
      headerTitle:  'Profile',
    }
  }
);
/*const AppStack = createStackNavigator({
   Home:HomeScreen,
   Chat:ChatScreen,
   Profile:ProfileScreen,
   CreateGroup:CreateGroup,
   Groups:Groups,
   GroupChat:GroupChatSingle,
   GroupOptions:GroupOptions,
   AddMember:AddGroupMembers,
   GroupMembers:GroupMembers});*/
const AuthStack = createStackNavigator({ Login: LoginScreen,Signup:Signup });
const testStack = createStackNavigator({ 
  Countries:    CountriesScreen,
  Cities:       CitiesScreen,
  Chat:         ChatScreen,
  Profile:      ProfileScreen,
  ChatByCity:   ChatCity,
  CreateGroup:  CreateGroup,
  GroupChat:    GroupChatSingle,
  AddMember:    AddGroupMembers,
  GroupMembers: GroupMembers,
  GroupOptions: GroupOptions,
  Settings:     SettingsScreen,
});
const AppStack = createBottomTabNavigator({
  Explore:  {
    screen:ExploreStack,
    navigationOptions:  {
      tabBarLabel:  'All',
      tabBarIcon: ({ tintColor }) => (
        tintColor == '#131e41' ?
        <Image
          source = {require('./images/icons/icon-all-bottom.png')} 
          style = {[styles.icon, {tintColor: tintColor}, {width: 24, height: 24}]}
        />
        :
        <Image
          source = {require('./images/icons/icon-all-bottom.png')}
          style = {[styles.icon, {tintColor: tintColor}, {width: 24, height: 24}]}
        />
      ),
      tabBarOptions: {
          activeTintColor: '#005ca4',
      }
    }
  },
  Home: {
    screen: HomeStack,
    navigationOptions:  {
      tabBarLabel:  'Public',
      tabBarIcon: ({ tintColor }) => (
        tintColor == '#131e41' ?
        <Image
          source = {require('./images/icons/icon-public-bottom.png')} 
          style = {[styles.icon, {tintColor: tintColor},{width: 25,height: 24}]}
        />
        :
        <Image
          source = {require('./images/icons/icon-public-bottom.png')}
          style = {[styles.icon, {tintColor: tintColor},{width: 25,height: 24}]}
        />
      ),
      tabBarOptions: {
          activeTintColor: '#005ca4',
      }
    }
  },
  Groups: {
    screen:  GroupStack,
    navigationOptions: {
      tabBarLabel:  'Private',
      tabBarIcon: ({ tintColor }) => (
        tintColor == '#131e41' ?
        <Image
          source = {require('./images/icons/icon-private-bottom.png')} 
          style = {[styles.icon, {tintColor: tintColor}, {width: 18,height: 24}]}
        />
        :
        <Image
          source = {require('./images/icons/icon-private-bottom.png')}
          style = {[styles.icon, {tintColor: tintColor}, {width: 18,height: 24}]}
        />
      ),
      tabBarOptions: {
          activeTintColor: '#005ca4',
      }
    }
  },
  Profile:  {
    screen: ProfileStack,
    navigationOptions:  {
      tabBarLabel:  'My Groups',
      tabBarIcon: ({ tintColor }) => (
        tintColor == '#005ca4' ?
        <Image
          source = {require('./images/icons/icon-users-bottom.png')} 
          style = {[styles.icon, {width: 25, height: 24}]}
        />
        :
        <Image
          source = {require('./images/icons/icon-users-bottom.png')}
          style = {[styles.icon, {width: 25, height: 24}]}
        />
      ),
      tabBarOptions: {
          activeTintColor: '#005ca4',
      }
    }
  },
  /*
  Orders:{
  screen:OrdersHome,
  navigationOptions:{
    tabBarLabel:'Orders',
    tabBarIcon: ({ tintColor }) => (tintColor == '#69e180' ?
    <Image
      source={require('./src/images/tabicons/shopping-cart-active.png')} 
      style={[styles.icon, {tintColor: tintColor},{width:24,height:24}]}
    />
    :
    <Image
      source={require('./src/images/tabicons/shopping-cart-inactive.png')}
      style={[styles.icon, {tintColor: tintColor},{width:24,height:24}]}
    />
  ),
  tabBarOptions: {
      activeTintColor: '#69e180',
  }
  }
  },
  More:{
  screen:MoreInfoStack,
  navigationOptions:{
    tabBarLabel:'more',
    tabBarIcon: ({ tintColor }) => (tintColor == '#69e180' ?
    <Image
      source={require('./src/images/tabicons/more-inactive.png')} 
      style={[styles.icon, {tintColor: tintColor},{width:24,height:24}]}
    />
    :
    <Image
      source={require('./src/images/tabicons/more-inactive.png')}
      style={[styles.icon, {tintColor: tintColor},{width:24,height:24}]}
    />
  ),
  tabBarOptions: {
      activeTintColor: '#69e180',
  }
  }
  }*/


})

class CustomDrawerContentComponent extends Component {
  state = {
    name: User.name,
    email: User.email,
    avatar: User.avatar,
    password: User.password
  }

  constructor(props) {
    super(props);
    User.name != "" ? this._setAsyncStorage() : this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    this.state.email= await AsyncStorage.getItem('userEmail');
    this.state.name = await AsyncStorage.getItem('userName');
    this.state.avatar = await AsyncStorage.getItem('userAvatar');
    
    User.name = this.state.name;
    User.avatar = this.state.avatar;
    User.email = this.state.email;
  }

  _setAsyncStorage = async () => {
    await AsyncStorage.setItem('userEmail', User.email);
    await AsyncStorage.setItem('userName', User.name);
    await AsyncStorage.setItem('userAvatar', User.avatar);
  }

  componentWillMount() {
    this._isMounted = true;
    console.log
    firebase.database().ref('/users/' + base64.encode(User.email)).once('value').then(function (snapshot) {
      var username = (snapshot.val() && snapshot.val().name);
      User.name = username;
      User.avatar = snapshot.val().avatar;
      console.log('username from login db',User.name)
      // console.log('Username log from login',User.name);
      if(this._isMounted){
        this.setState({username: username})  
        this.setState({avatar: User.avatar}); 
      }
    });
    // alert("state name" + this.state.name);
  }

  render() {
    return (
      <Container>
        <View style={{height: 265, width: 320, backgroundColor: '#fff'}}>
          <Body style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Image 
                style={styles.drawerImage}  
                source={require('./images/sidebar-header.png')} />
              <TouchableOpacity style={{position: 'absolute', top: 10, left: 10}} onPress={()=> this.props.navigation.goBack()}>
                <Image source={require('./images/btn_back.png')} style={{width: 31, height: 20, marginTop: 10, marginRight: 10}} />
              </TouchableOpacity>
              <View style={{position: 'absolute', justifyContent: "center", alignContent: "center", textAlign: 'center', top: 50, left: 100}}>
                <Thumbnail source={{uri: User.avatar}} style={{width: 100, height: 100, marginRight: 5,  borderRadius: Platform.OS === 'ios' ? 50:60}}/>    
                {/* <Thumbnail source={ require('./images/icon-create-group.png')} style={{width: 100, height: 100, marginRight: 5}}/>     */}
                {/* <Text>{this.props.navigation.getParam("name", null)}</Text> */}
                <Text style={{width: 100, textAlign: 'center', color: 'white', fontSize: 20}}>{User.name}</Text>
              </View>
          </Body>
        </View>
        <Content style={{paddingLeft: 30, fontSize: 25}}>
          <DrawerItems {...this.props} style={{paddingLeft: 10}}/>
        </Content>
      </Container>    
    )
  }
}

const AppDrawerNavigator = createDrawerNavigator({
  Explore: {
    screen: testStack,
    navigationOptions: () => ({
      title: 'Explore',
      drawerIcon: ({tintColor}) => (
        <Image source={require('./images/icons/icon-clock-active.png')} style={{height: 24, width: 24}}/>
      )
    })
  }, 
  ChatRoom: {
    screen: ChatCity,
    navigationOptions: () => ({
      title: 'Chat Room',
      drawerIcon: () => (
        <Image source={require('./images/icons/icon-noti-active.png')} style={{height: 24, width: 24}}/>
      )
    })
  },
  Profile: {
    screen: ProfileScreen,
    navigationOptions: () => ({
      title: 'My Profile',
      drawerIcon: () => (
        <Image source={require('./images/icons/icon-user-active.png')} style={{height: 24, width: 24}} />
      )
    })
  },
  Settings: {
    screen: SettingsScreen,
    navigationOptions: () => ({
      title: 'Settings',
      drawerIcon: () => (
        <Image source={require('./images/icons/icon-setting-active.png')} style={{height: 24, width: 24}} />
      )
    })
  },
  Share: {
    screen: SettingsScreen,
    navigationOptions: () => ({
      title: 'Share',
      drawerIcon: () => (
        <Image source={require('./images/icons/icon-share-active.png')} style={{height: 24, width: 24}} />
      )
    })
  },
  Logout: {
    screen: AuthStack,
    navigationOptions: () => ({
      title: 'Log Out',
      drawerIcon: () => (
        <Image source={require('./images/icons/icon-logout-active.png')} style={{height: 24, width: 24}} />
      )
    })
  }
},
{
  initialRouteName: 'Explore',
  contentComponent: CustomDrawerContentComponent,
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoure: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle',
  drawerWidth: 320,
  contentOptions: {
    activeTintColor: '#02406C',
    inactiveTintColor: '#02406C',
    activeBackgroundColor: '#EBEBEB',
    inactiveBackgroundColor: '#fff',
    itemsContainerStyle: {
    },
    itemStyle: {
      height: Platform.OS === 'android' ? 40 : 55,
      borderBottomLeftRadius: 60,
      borderTopLeftRadius: 60,
      fontSize: 20, 
    },
    labelStyle: {
      fontSize: 20, 
      fontWeight: 'normal',
      color: '#02406C'
    },
    activeLabelStyle: {
      fontSize: 20, 
      fontWeight: 'normal'
    },
  }
})

const MainNavigator = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: {
      screen: AppDrawerNavigator
    },
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));

// const MainNavigator = createAppContainer(createDrawerNavigator({
//   AuthLoading: AuthLoadingScreen,
//   App: testStack,
//   Auth: AuthStack,
// },
// {
//   initialRouteName: 'AuthLoading',
// }));

export default class App extends Component {

  // Add news in global variable and local storage
  addNewsNotification(object) {
    let news = NewsNote.news;
    let flagNew = true;
    Object.keys(news).map(value => {
      if (news[value].email == object.from) {
        news[value].timestamp = object.timestamp;
        news[value].count++;
        news[value].latestMsg = object.message;
        flagNew = false;
      }
    })
    NewsNote.news = news;
    if (flagNew) {
      NewsNote.news.push({
        email: object.from,
        count: 1,
        latestMsg: object.message,
        timestamp: object.timestamp
      })
      NewsNote.isExist = true;
    }
    alert(JSON.stringify(NewsNote))
    AsyncStorage.setItem({notification: NewsNote});
  }

  // get new message
  getNewMessages() {
    let dbRef6 = firebase.database().ref('messages').child(base64.encode(User.email));
    dbRef6.on('child_added', (val) => {
      let currentTime = (new Date).getTime();
      // alert(currentTime);
      let objects = val.val();
      if (objects != null && objects != undefined) {
        // alert(JSON.stringify(objects))
        Object.keys(objects).map(value => {
          if (objects[value].from != User.email) {
            if (currentTime < objects[value].createdAt * 10) {
              this.addNewsNotification(objects[value]);
              // alert("notification");
            }
          }
        });
      }
      this.addNewsNotification(s)
    });
  }
  async componentWillMount() {
    let fcmToken = await firebase.messaging().getToken();
    alert(fcmToken);
  }

  componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();

    // let timer = setInterval(this.getNewMessages, 5000);
    // this.setState({timer: timer});
  }

  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();

    this.clearInterval(this.state.timer);
  }

  async createNotificationListeners() {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const {title, body} = notification;
      this.showAlert(title, body);
    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    });

    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    }

    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log(Json.stringify(message));
    })
  }

  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
        { text: "ok", onPress: () => console.log('Ok Pressed')},
      ],
      { cancelable: false },
    );
  }
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if( enabled ) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if( !fcmToken ) {
      fcmToken = await firebase.messaging().getToken();
      if( fcmToken ) {
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  render() {
    return (    
      <MainNavigator/>      
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#ffffff'
  },
  button:{
    width:80,
    height:80,
    borderRadius:40,
    justifyContent:'center',
    alignItems:'center',
  },
  buttonTitle:{
      fontSize:18,   
  },
  buttonBorder:{
    width:60,
    height:60,
    borderRadius:38,
    borderWidth:0.5,
    borderColor:'#ffffff',
    justifyContent:'center',
    alignItems:'center'
  },
  iconhome:{
    paddingBottom:45,
  },
  imgBackground: {
    width: 320,
    height: '25%',
    flex: 1,
    alignItems:'center'
  }, 
  drawerImage: {
    width: 320, 
    height: 265
  }
});

