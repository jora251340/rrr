import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage, Platform, Dimensions, ImageBackground, Image, TouchableOpacity, Alert } from 'react-native';
import { Container, Content, Form, Item, Input, Label, Icon ,Picker} from 'native-base';
import firebase from 'firebase';
import ImagePicker from 'react-native-image-picker';
import LinearGradient from 'react-native-linear-gradient';
import base64 from 'react-native-base64';
import uuid from 'react-native-uuid';
import RNFetchBlob from 'rn-fetch-blob'

import User from '../User';
import firebaseSDK from '../firebaseSDK';

// get device's infos
const deviceWidth = Dimensions.get('screen').width;

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

export default class Signup extends Component {
  static navigationOptions = {
    headerStyle: { backgroundColor: '#005497' },
    headerTitleStyle: { color: '#ffffff' },
    headerTintColor: '#ffffff',
  }
  state = {
    name:'',
    email: '',
    password:'',
    avatar:'',
    imageName: '',
    imageSource: '',
    imagePath: null,
    loading: true,
    response:null,
    progress:null,
    countries : [],
    country:'',
    cities : [],
    city:[],
    uploading:'Upload Avatar ',
   // uploading:null,
  };

  componentWillMount() {
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email: val })
      }
    })
    firebase.database().ref('/countries').once('value', (snapshot) => {
      const countries = Object.values(snapshot.val());
        this.setState({countries :countries })
    })
  }

  uploadImage(uri, mime) {
    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'android' ? uri.replace('file://', '') : uri
      let uploadBlob = null
      // let imagename = uuid
      const imageRef = firebase.storage().ref('avatars').child(uuid())
      fs.readFile(uploadUri, 'base64')
        .then((data) => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then((blob) => {
          uploadBlob = blob
          this.setState({ uploading: 'Uploading...' })
          return imageRef.put(blob, { contentType: mime })
        })
        .then(() => {
          uploadBlob.close()
          return imageRef.getDownloadURL()
        })
        .then((url) => {
          resolve(url)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
  
  onPressCreate = async () => {
    if (this.state.email.length === 0) {
      Alert.alert('Error', ' Email cannot be empty')
    } else if (this.state.password.length === 0) {
      Alert.alert('Error', ' Password cannot be empty')
    } else if (this.state.name.length === 0) {
      Alert.alert('Error', ' Name cannot be empty')
    } else if (this.state.avatar.length === 0) {
      Alert.alert('Error', 'Please Upload Avatar')
    } else if (this.state.email.length < 5) {
      Alert.alert('Error', 'wrong Email')
    } else if (this.state.password.length < 3) {
      Alert.alert('Error', 'Wrong Password')
    } else {
      try {
        let encEmail = base64.encode(this.state.email)
        const user = {
          name: this.state.name,
          email: this.state.email,
          password: this.state.password
        };
      
        await AsyncStorage.setItem('userEmail', this.state.email);
        await AsyncStorage.setItem('userName', this.state.name);
        await AsyncStorage.setItem('userAvatar', this.state.avatar);

        User.email = this.state.email;
        firebase.auth().createUserWithEmailAndPassword(user.email, user.password).then(() => {
          console.log('Signup successful.');
          this.setState({
            response: 'Account Created!'
          })
          // firebase.createAccount(user);
          firebase.database().ref('users/' + encEmail).set({ name: user.name, email: this.state.email, avatar: this.state.avatar,country:this.state.country,city:this.state.city })
          alert('user Created Successfully')
          
          const response = firebaseSDK.login(
            user,
            this.loginSuccess,
            this.loginFailed
          );
        })
        .catch((error) => {
          alert(error.code);
          alert(error.message);
        });      
      } catch ({ message }) {
        alert('create account failed. catch error:' + message);
      }
    }  
  };

  onChangeTextEmail = email => this.setState({ email });
  onChangeTextPassword = password => this.setState({ password });
  onChangeTextName = name => this.setState({ name });
  onChangeCountry = country => this.setState({ country:country });
  getCities(country)
  {
    firebase.database().ref('/cities').once('value', (snapshot) => {
      const cities = Object.values(snapshot.val());
        this.setState({cities :cities.filter(item => (item.countryname == country) ) })
    })
  }

  loginSuccess = () => {
    this.props.navigation.navigate('Explore', {
      name: this.state.name,
      email: this.state.email,
      avatar: this.state.avatar,
    });

    User.email = this.state.email;
    User.name = this.state.name;
    User.avatar = this.state.avatar;
    let encEmail = base64.encode(User.email, 'user object email');
    const newemail = User.email;
    this.props.navigation.navigate('Explore', { UserEmailnew: newemail });
  };

  onImageUpload = async () => {
    try {
      var options = {
        title: 'Select Avatar',
        customButtons: [
          { name: 'fb', title: 'Choose Photo from Facebook' },
        ],
        storageOptions: {
          skipBackup: true,
          path: 'images'
        }
      };
      await ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          console.log("uploading");
          this.uploadImage(response.uri, response.type)
            .then(url => {
            this.setState({ avatar: url, uploading:'Uploaded' }) })
            .catch(error => console.log(error))
        }
      });
    } catch (err) {
      console.log('onImageUpload error:' + err.message);
      alert('Upload image error:' + err.message);
    }
  };

  render() {
    const placeholder = {
      label: 'Select a country...',
      value: null,
      color: '#9EA0A4',
    };
    const { uploading, imgSource, progress, images } = this.state;
    return (
      <Container>
        <ImageBackground style={styles.imgBackground}
          resizeMode='cover'
          source={require('../images/header2.png')}>
          <Content>
            <Form style={{ alignItems: 'center' }}>
              <Image source={require('../images/logo.png')} style={{ width: 150, height: 150, marginTop: 70 }} />
              <Item fixedLabel>
                <Label>Email</Label>
                <Input
                  placeHolder="test@live.com"
                  onChangeText={this.onChangeTextEmail}
                  value={this.state.email}
                />
              </Item>
              <Item fixedLabel last>
                <Label>Password</Label>
                <Input
                  onChangeText={this.onChangeTextPassword}
                  value={this.state.password}
                />
              </Item>
              <Item fixedLabel last>
                <Label>Name</Label>
                <Input
                  onChangeText={this.onChangeTextName}
                  value={this.state.name}
                />
              </Item>
              <Item picker>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  placeholder="Select Group Type"
                  placeholderStyle={{ color: "#bfc6ea" }}
                  placeholderIconColor="#007aff"
                  selectedValue={this.state.country}
                  onValueChange={(itemValue, itemIndex) =>{
                    this.setState({country: itemValue})
                    this.getCities(itemValue)
                  }}>
                  <Picker.Item style={{textAlign: 'center'}} label="Select country" value="" />
                  {this.state.countries.map((country,i) => {
                    return <Picker.Item style={{textAlign: 'center'}} label={country.name} value={country.name} key={i}/>}
                  )}
                </Picker>
              <Picker
                  selectedValue={this.state.city}
                  style={styles.inputBox}
                  onValueChange={(itemValue, itemIndex) => {
                        this.setState({city: itemValue})
                  }
              }>
                <Picker.Item style={{textAlign: 'center'}} label="Select city" value="" />
                {this.state.cities.map((city,i) => {
                  return <Picker.Item style={{textAlign: 'center'}} label={city.name} value={city.name} key={i}/>}
                )}
              </Picker>
            </Item>
            <Item picker>
             </Item>
              <View style={styles.btnContainer}>
                <TouchableOpacity onPress={this.onImageUpload}>
                  <LinearGradient start={{ x: 0.5, y: 0.25 }} end={{ x: 0.5, y: 1.8 }}
                                  locations={[0, 0.6]}
                                  colors={['#005da5', '#007ddf']} style={styles.startScanBtn}>
                    <Text style={styles.buttonText}>
                      {this.state.uploading}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
              <View style={styles.btnContainer}>
                <TouchableOpacity onPress={this.onPressCreate}>
                  <LinearGradient start={{ x: 0.5, y: 0.25 }} end={{ x: 0.5, y: 1.8 }}
                                  locations={[0, 0.6]}
                                  colors={['#005da5', '#007ddf']} style={styles.startScanBtn}>
                    <Text style={styles.buttonText}>
                      Sign up
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </Form>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

const offset = 16;
const styles = StyleSheet.create({
  title: {
    marginTop: offset,
    marginLeft: offset,
    fontSize: offset
  },
  nameInput: {
    height: offset * 2,
    margin: offset,
    paddingHorizontal: offset,
    borderColor: '#111111',
    borderWidth: 1,
    fontSize: offset
  },
  buttonText: {
    marginLeft: offset,
    fontSize: 42
  },
  btn: {
    width: deviceWidth - 20,
    justifyContent: 'center'
  },
  btnContainer: {
    marginTop: offset,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 10,
  },
  imgBackground: {
    width: deviceWidth,
    height: '25%',
    flex: 1,
    alignItems: 'center'
  },
  startScanBtn: {
    width: deviceWidth - 20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
  progressBar: {
    backgroundColor: 'rgb(3, 154, 229)',
    height: 3,
    shadowColor: '#000',
  }
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#000000',
    borderRadius: 4,
    color: '#000000',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: '#000000',
    borderRadius: 8,
    color: '#000000',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputBox: {
    width: '100%',
    height: 45,
    backgroundColor: '#1c2635',
    paddingHorizontal: 20,
    fontSize:16,
    color: '#ffffff',
    marginVertical: 10,
    //textAlign: 'center'
  },
});