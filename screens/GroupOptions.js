import React, { Component } from 'react';
import { View, SafeAreaView, AsyncStorage, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Icon, Right,Left,Thumbnail,Body } from 'native-base';
import User from '../User';
import styles from '../constants/styles';
import firebase from 'firebase';
import base64 from 'react-native-base64';
export default class GroupOptions extends React.Component {
    _isMounted = false;
    static navigationOptions = {
        title: 'Group Options'
    }
    constructor(props) {
      super(props);
      this.state = {
        Groupinfo: {
          groupname:props.navigation.getParam('Groupname'),
          groupid:props.navigation.getParam('GroupId'),
        },           
      }
    }
    
    render() {
        return (          
          <Container>
            <Content>
              <Card>
                <CardItem header button onPress={() => alert("This is Card Header")}>
                  <Left>
                    <Thumbnail source={require('../images/user.png')} />
                    <Body>
                      <Text>{this.state.Groupinfo.groupname}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem button onPress={()=>this.props.navigation.navigate('AddMember',{
                          GroupId:this.state.Groupinfo.groupid,
                          Groupname:this.state.Groupinfo.groupname,
                })}>
                  <Icon active name="person-add" />
                  <Text>Add New Member</Text>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </CardItem>
                <CardItem button onPress={()=>this.props.navigation.navigate('GroupMembers',{
                          GroupId:this.state.Groupinfo.groupid,
                          Groupname:this.state.Groupinfo.groupname,
                })}>
                  <Icon active name="people"  />
                  <Text>Group Members</Text>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </CardItem>
                <CardItem>
                  <Icon active name="exit" />
                  <Text>Leave Group</Text>
                  <Right>
                    <Icon name="arrow-forward" />
                  </Right>
                </CardItem>
              </Card>
            </Content>
        </Container>
      )
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
}