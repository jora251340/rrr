import React, { Component } from 'react';
import { FlatList, Image, AsyncStorage, TouchableOpacity, Text, View, StyleSheet, Dimensions, ImageBackground, StatusBar } from 'react-native';
import { Container, Spinner } from 'native-base';
import firebase from 'firebase';

import User from '../User';

const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;

export default class CountriesScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      header:null,      
    }   
  }
  state = {
    loading:true,
    countries: [],
    email:'',
  }
  
  getInitialState() {
    return({
      state: {
        loading:true,
        countries: [],
        email:'',
      }
    })
  }

  componentWillMount() {
    AsyncStorage.getItem('userEmail').then( val => {
      if (val) {
        this.setState({ email:val })
        console.log('email in storage', this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email=noQuotes;
        console.log('new user email in func', User.email)
      }
    })
    console.log('new user email out func',User.email)
    let dbRef = firebase.database().ref('countries');
    dbRef.on('child_added', (val) => {
    let country = val.val();
      this.setState((prevState) => {
        return {
          loading:false,
          countries: [...prevState.countries, country]
        }
      })
    })
  }
  
  renderRow = ({ item }) => {
    return (    
      <TouchableOpacity style={[styles.chatUserBox,shadow]} onPress={() => this.props.navigation.navigate('Cities', item)}>
        <Text style={styles.countryName}>{item.name}</Text>
      </TouchableOpacity>
    )
  }
  render() {
    if (this.state.loading) {
      return (
        <ImageBackground style={styles.imgBackground } 
                        resizeMode='cover' 
                        source={require('../images/header2.png')}>
          <TouchableOpacity style={{marginTop:15,marginLeft:deviceWidth-70}} onPress={ () => this.props.navigation.toggleDrawer() }>
            <Image source={require('../images/menu-new.png')} style={{width:20,height:20, marginTop: 20, marginRight: 10}} />
          </TouchableOpacity>
          <TouchableOpacity style={{marginTop:-28,marginRight:deviceWidth-70}}>
            <Image source={require('../images/bell-new.png')} style={{width:20, height:25, marginTop: 10, marginLeft: 10}} />
          </TouchableOpacity>       
          <StatusBar backgroundColor="#005497" barStyle="light-content"/>
          <View style={{alignItems: 'center'}}> 
            <Image source={require('../images/logo.png')} style={{width: 200, height: 200,marginTop: -30}} />
            <Text>Loading Countries..</Text>
            <Spinner />
          </View>
        </ImageBackground>
      );
    }
    return (
      <ImageBackground style={styles.imgBackground } 
                        resizeMode='cover' 
                        source={require('../images/header2.png')}>
       {/* <OfflineNotice /> */}
          <TouchableOpacity style={{marginTop: 15, marginLeft: deviceWidth-70}} onPress={ () => this.props.navigation.toggleDrawer() }>
            <Image source={require('../images/menu-new.png')} style={{width: 20,height: 20, marginTop: 20, marginRight: 10}} />
          </TouchableOpacity>
          <TouchableOpacity style={{marginTop: -28, marginRight: deviceWidth-70}}>
            <Image source={require('../images/bell-new.png')} style={{width: 20 ,height: 25, marginTop: 10, marginLeft: 10}} />
          </TouchableOpacity>     
          <StatusBar backgroundColor="#005497" barStyle="light-content"/>
          <View style={{alignItems: 'center'}}> 
            <Image source={require('../images/logo.png')} style={{width: 200,height: 200,marginTop: -30}} />
            <Container style={[styles.container, shadow_box]}>
              <FlatList
                data={this.state.countries}
                renderItem={this.renderRow}
                keyExtractor={(item) => item.key}
                style={styles.chatList}
              />       
            </Container>
          </View>
        </ImageBackground>
    );
  }
}
const offset = 16;
const shadow ={
  shadowColor: '#005ca4',
  shadowRadius: 10,
  shadowOpacity: 0.3,
  elevation: 5,
  shadowOffset: {width: 0, height: 2}
}
const shadow_box ={
  shadowColor: '#005ca4',
  shadowRadius: 10,
  shadowOpacity: 0.4,
  elevation: 15,
  shadowOffset: { width: 0, height: 10 }
}
const styles = StyleSheet.create({
    container: {
      width: deviceWidth-40,
      height: 0,
      borderRadius: 10,
      backgroundColor: '#ffffff',
      justifyContent: 'center',
      marginTop: -30,
      marginBottom: 10,
      marginLeft: 20,
      marginRight: 20,
      padding: 10
    },
    title: {
      marginTop: offset,
      marginLeft: offset,
      fontSize: offset
    },
    nameInput: {
      height: offset * 2,
      margin: offset,
      paddingHorizontal: offset,
      borderColor: '#111111',
      borderWidth: 1,
      fontSize: offset
    },
    buttonText: {
        marginLeft: offset,
        fontSize: 42
    },
    btn:{
      width: deviceWidth-20,
      justifyContent: 'center'
    },
    btnContainer:{
      marginTop: offset,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      padding: 10,
    },
    imgBackground: {
      width: deviceWidth,
      height: '25%',
      flex: 1,
      alignItems:'center'
  },
  startScanBtn: {
    width: deviceWidth-20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
  chatList:{
    marginLeft: 5,
    marginRight: 5,
    width: deviceWidth-70,
    height: deviceHeight-300,
    marginBottom: 40
  },
  chatUserBox:{
    width: deviceWidth-80,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5,
    padding: 10
  },
  btnbadge:{
    width: 30,
    height: 30,
    justifyContent: 'center',
    padding: 5,
    borderRadius: 15,
    marginTop: 5,
  },
  chatTime:{
    color: '#005ca4',
    fontSize: 11,
  },
  countryName:{
    color: '#282829',
    fontSize: 16,
    textAlign: 'center',
  },
  chatText:{
    color: '#9e9e9e',
    fontSize: 16,
  }
});
