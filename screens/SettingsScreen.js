import React, { Component } from 'react';
import { Image, TouchableOpacity, Text, View, StyleSheet, Dimensions, ImageBackground } from 'react-native';
import { Container, ListItem, Left, Right } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

// get device's infos
const deviceWidth = Dimensions.get('screen').width;

export default class SettingsScreen extends Component {  
  static navigationOptions =({navigation}) => {
    return {
      header: null,      
    }   
  }
  
  render() {
    return (
      <ImageBackground style={styles.imgBackground} 
                      resizeMode='cover' 
                      source={require('../images/header2.png')}>
        <TouchableOpacity style={{marginTop: 30, marginRight: deviceWidth-60,}} onPress={()=>this.props.navigation.goBack()}>
          <Image source={require('../images/btn_back_2.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 20}}/>
        </TouchableOpacity>
        <View style={styles.headerTitle}>
          <Text style={styles.headerTitleText}>Settings</Text>
        </View>
        <View> 
          <Container style={[styles.mainDisplay, shadow]}>
              <ListItem>
                  <Left>
                      <Text style={styles.chatText}>Account</Text>
                  </Left>
              </ListItem>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Edit Profile</Text>
                      </Left>
                      <Right>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info1.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Location Sharing</Text>
                      </Left>
                      <Right style={{flexDirection: 'row', justifyContent:'flex-end',alignItems: 'center',}}>
                          {/* <Image style={{}} source={require('../images/')} /> */}
                          <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                                          locations={[0,0.6]}
                                          colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
                              <Text style={{color: '#ffffff'}}>2</Text>
                          </LinearGradient>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info2.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Linked Accounts</Text>
                      </Left>
                      <Right style={{flexDirection: 'row', alignItems:'center', justifyContent:'flex-end'}}>
                          {/* <Image style={{}} source={require('../images/')} /> */}
                          <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                                          locations={[0,0.6]}
                                          colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
                              <Text style={{color: '#ffffff'}}>0</Text>
                          </LinearGradient>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
              <ListItem>
                  <Left>
                      <Text style={styles.chatText}>Security</Text>
                  </Left>
              </ListItem>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info3.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Change Password</Text>
                      </Left>
                      <Right>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info4.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Privacy</Text>
                      </Left>
                      <Right>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info2.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Linked Accounts</Text>
                      </Left>
                      <Right>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
              <ListItem>
                  <Left>
                      <Text style={styles.chatText}>System</Text>
                  </Left>
              </ListItem>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                  <ListItem>
                      <Left>
                          <Image style={styles.settings_icon} source={require('../images/icons/Info3.png')} />
                          <Text style={[styles.chatName, {marginLeft: 10}]}>Passcode Lock</Text>
                      </Left>
                      <Right>
                          <Image style={styles.settings_arrow} source={require('../images/settings-arrow.png')} />
                      </Right>
                  </ListItem>
              </TouchableOpacity>
          </Container>
        </View>
      </ImageBackground>
    );
  }  
}

const shadow = {
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:15,
  shadowOffset:{width:0,height:10}
}

const styles = StyleSheet.create({
  chatName: {
    color: '#282829',
    fontSize: 16,
    textAlign: 'left'
  },
  btnbadge: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 5,
    borderRadius: 15,
    marginTop: 5,
    marginRight: 10
  },
  imgBackground: {
    width: deviceWidth,
    height: '25%',
    flex: 1,
    alignItems: 'center'
  },
  mainDisplay: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    marginTop: 10,
    width: deviceWidth-30,
  },
  chatText: {
    color: '#005ca4',
    fontSize: 20,
  },
  chatBackBtn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginLeft: 5,
    padding: 5, 
  },
  chatUserBox: {
    width: deviceWidth-60,
    height: 80,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 30,
    padding: 10
  },
  headerTitleText: {
    color: '#ffffff',
    fontSize: 25
  },
  headerTitle: {
    marginTop: -30,
    color: '#ffffff',
    textAlign: 'center',
    alignItems: 'center',
    width: 200,
  },
  settings_arrow: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 10,
    height: 16,
  },
  listtext: {
    color: '#282829',
    fontSize: 18,
    alignItems: 'flex-start'
  }, 
  settings_icon: {
    width: 25,
    height: 25,
  }
})