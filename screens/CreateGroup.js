import React, { Component } from 'react';
import { Platform, Image,AsyncStorage, TouchableOpacity, Alert, Text, View,StyleSheet,Dimensions,ImageBackground,StatusBar } from 'react-native';
import { Container, Content, ListItem, Left, Body, Right, Thumbnail ,Form, Button, Label, Picker, Textarea, Input, Icon} from 'native-base';
import firebase, { firestore } from 'firebase';
import base64 from 'react-native-base64';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob'
import uuid from 'react-native-uuid';

import User from '../User';
import OfflineNotice from './OfflineNotice';

//const emailnew = ''
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
//Prepare Blob support
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob
const shadowOpt = {
	width:deviceWidth-50,
  color:"#000",
  height:85,
	border:1,
	radius:5,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:3,alignItems:'center',backgroundColor:'#ffffff'}
}
export default class CreateGroup extends Component {  
  static navigationOptions =({navigation}) => {
    return {
      header:null,      
    }   
  }
  state = {
    changeName: 0,
    checked: 1,
    groupname: '',
    groupavatar: '',
    country:undefined,
    cstate:'',
    imageName: 'avatar-new',
    imageSource: '',
    imagePath: null,
    loading: true,
    email:'',
    grouptype:undefined,
    description:'',
    response:null,
    progress:null,
    countries : [],
    country:'',
    cities : [],
    city:[],
    uploading:'Upload Group Avatar ',
   // selected2: undefined
  };
  componentWillMount() {
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
          this.setState({ email: val })
      }
    })
    firebase.database().ref('/countries').once('value', (snapshot) => {
      const countries = Object.values(snapshot.val());
        this.setState({countries :countries })
    })
  }
  onChangeCountry = country => this.setState({ country:country });
  getCities(country) {
    firebase.database().ref('/cities').once('value', (snapshot) => {
      const cities = Object.values(snapshot.val());
        this.setState({cities :cities.filter(item => (item.countryname == country) ) })
    })
  }
  onValueChange2(value) {
    this.setState({
      country: value
    });
  }
  onValueChange3(value) {
    this.setState({
      grouptype: value
    });
  }
  uploadImage(uri, mime ) {
    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'android' ? uri.replace('file://', '') : uri
      let uploadBlob = null
     // let imagename = uuid
      const imageRef = firebase.storage().ref('groupavatars').child(uuid())

      fs.readFile(uploadUri, 'base64')
        .then((data) => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then((blob) => {
          uploadBlob = blob
          this.setState({ uploading: 'Uploading...' })
          return imageRef.put(blob, { contentType: mime })
        })
        .then(() => {
          uploadBlob.close()
          return imageRef.getDownloadURL()
        })
        .then((url) => {
          resolve(url)
        })
        .catch((error) => {
          reject(error)
      })
    })
  }
  onPressCreate = async () => {
    if (this.state.groupname.length === 0) {
      Alert.alert('Error', ' Groupname cannot be empty')
    }
    else if (this.state.country.length === 0) {
      Alert.alert('Error', ' country cannot be empty')
    } 
    else  if (this.state.city.length === 0) {
      Alert.alert('Error', ' City cannot be empty')
    } 
    else  if (this.state.description.length === 0) {
      Alert.alert('Error', ' description cannot be empty')
    } 
    else  if (this.state.groupavatar.length === 0) {
      Alert.alert('Error', 'Please Upload Group Avatar')
    } 
    else if (this.state.description.length <20) {
      Alert.alert('Error', 'wrong description')
    } else if (this.state.grouptype.length === 0) {
      Alert.alert('Error', 'Group Type Cannot be empty')
    } else {
      try {
       // let encEmail = base64.encode(this.state.email)
        let GroupId  = firebase.database().ref('Groups').push().key
        console.log('GroupId',GroupId);
        let GroupCreator = base64.encode(User.email)
        console.log('GroupCreator',GroupCreator);
        const Group = {
            Groupname: this.state.groupname,
            Country: this.state.country,
            city:this.state.city,
           // state: this.state.cstate,
            GroupAvatar:this.state.groupavatar,
            Groupid:GroupId,
            Grouptype:this.state.grouptype,
            GroupDescription:this.state.description,
        };
        console.log('group name is',Group.Groupname)
        // await AsyncStorage.setItem('userEmail', this.state.email);
        // User.email = this.state.email;
        // firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
        // firebase.createAccount(user);
        firebase.database().ref('Groups/' + GroupId).set({ groupname: Group.Groupname, countryname: Group.Country,cityname:this.state.city, avatar:Group.GroupAvatar,createdAt:firebase.database.ServerValue.TIMESTAMP,CreatedBy:GroupCreator,GroupDescription:Group.GroupDescription,Grouptype:Group.Grouptype })
        alert('Group Created Successfully')
        firebase.database().ref('Groups/' + GroupId + '/GroupMembers/'+GroupCreator).set({ membername:User.name, memberemail:GroupCreator, memberavatar:'https://firebasestorage.googleapis.com/v0/b/chatapp-f57d9.appspot.com/o/groupavatars%2Favatar.png?alt=media&token=2060c383-edc4-484b-a593-9d9344685bc4' })
        this.setState({
          groupname: null,
          groupavatar:null,
          country:undefined,
          groupname:undefined,
          cstate:null,
          imageName: null,
          imageSource: null,
        })
      } catch ({ message }) {
        console.log('Failed to Create group error:' + message);
      }
    }
  };

  onChangeTextGroupname = groupname => this.setState({ groupname: groupname });
  onChangeTextCountry = country => this.setState({ country: country });
  onChangeTextState = cstate => this.setState({ cstate: cstate });
  onChangeDescription = description => this.setState({ description: description });

  onImageUpload = async () => {
    try {
      var options = {
        title: 'Select Avatar',
        customButtons: [
          {name: 'fb', title: 'Choose Photo from Facebook'},
        ],
        storageOptions: {
          skipBackup: true,
          path: 'images'
        }
      };
      await  ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
        if (response.didCancel) {
          console.log('User cancelled image picker');
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
          this.uploadImage(response.uri,response.type)
              .then(url => { 
                this.setState({groupavatar: url})
                this.setState({ avatar: url,uploading:'Uploaded' })
              })
              .catch(error => console.log(error))
        }
      });
    } catch (err) {
      console.log('onImageUpload error:' + err.message);
      alert('Upload image error:' + err.message);
    }
  };

  render() {
    const { checked } = this.state;
    return (
      <ImageBackground style={styles.imgBackground } 
                      resizeMode='cover' 
                      source={require('../images/header2.png')}>
        <OfflineNotice />
        <TouchableOpacity style={{marginTop:20,marginRight:deviceWidth-60,}} onPress={()=>this.props.navigation.goBack()}>
          <Image source={require('../images/btn_back_2.png')} style={{width: 20, height: 25, marginRight: 10, marginLeft: 10}}/>
        </TouchableOpacity>
        <View style={styles.headerTitle}>
          <Text style={styles.headerTitleText}>Create Group</Text>
        </View>
        <StatusBar backgroundColor="#005497" barStyle="light-content"/>
        <View > 
          <Container style={[styles.mainDisplay,shadow]}>
            <Content style={styles.chatList}>
              <Form>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={{}} onPress={this.onImageUpload}>
                    {this.state.groupavatar.length == 0 ?
                      <Image source={require('../images/icon-create-group.png')} style={{width: 60, height: 60, marginRight: 10, marginLeft: 10}}/>
                    :
                      <Thumbnail source={{uri: this.state.groupavatar}} style={{width: 60, height: 60, marginRight: 10, marginLeft: 10}}/>
                    }
                  </TouchableOpacity>
                  {this.state.changeName == 0 ?
                    <TouchableOpacity onPress={() => {this.state.changeName = 1; this.forceUpdate();}}>
                      <Label style={[styles.chatText, {marginTop: 20}]}>
                        Group Name
                        <Image source={require('../images/icon-edit.png')} style={{width: 10, height: 10, marginLeft: 15, marginTop: -5}} />
                      </Label>
                    </TouchableOpacity>
                  :
                    <Input
                      placeholder="please enter Group Name"
                      onChangeText={this.onChangeTextGroupname}
                      value={this.state.groupname}
                    />
                  }
                </View>
                <View style={styles.blockstyle}>
                  <Label style={styles.chatText}>About
                    <Image source={require('../images/icon-edit.png')} style={{width: 10, height: 10, marginLeft: 5, marginTop: -5}} />
                  </Label>
                  <Textarea style={{height: 70}} rowSpan={5} placeholder="Description"  onChangeText={this.onChangeDescription}
                    value={this.state.description} />
                </View>
                <View style={[styles.blockstyle, {height: 50, width: '100%', flexDirection: 'row'}]}>
                  <TouchableOpacity style={{width: "50%", alignItems:'center', flexDirection: 'row', justifyContent: 'center'}} onPress={() => {this.state.checked = 1; this.forceUpdate();}}>
                    <Text style={styles.chatText}>Public</Text>
                    {this.state.checked == 2 ?
                      <Image source={require('../images/radiobox.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 10}}/>
                      :<Image source={require('../images/radiobox-active.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 10}}/>
                    }
                  </TouchableOpacity>
                  <TouchableOpacity style={{width: "50%", alignItems:'center', flexDirection: 'row', justifyContent: 'center'}} onPress={() => {this.state.checked = 2; this.forceUpdate()}}>
                    <Text style={styles.chatText}>Private</Text>
                    {this.state.checked == 1 ?
                      <Image source={require('../images/radiobox.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 10}}/>
                      :<Image source={require('../images/radiobox-active.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 10}}/>
                    }
                  </TouchableOpacity>
                </View>
                <View style={styles.blockstyle}>
                  <View style={{justifyContent: 'center', flexDirection: 'row', width: "100%"}}>
                    <View style={{width: "30%", justifyContent: 'center'}}>
                      <Text style={styles.chatText}>Country</Text>
                    </View>
                    <View style={{width: "70%"}}>
                      <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="arrow-down" />}
                        style={{ width: undefined, borderColor: '#005497', borderWidth: 1 }}
                        placeholder="Select Country"
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.country}
                        onValueChange={(itemValue, itemIndex) =>{
                          this.setState({country: itemValue})
                          this.getCities(itemValue)
                        }}>
                        <Picker.Item style={{textAlign: 'center'}} label="Select country" value="" />
                        {this.state.countries.map((country,i) =>
                        {
                          return <Picker.Item style={{textAlign: 'center'}} label={country.name} value={country.name} key={i}/>}
                        )}
                      </Picker>
                    </View>
                  </View>
                  <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                    <View style={{width: "30%", justifyContent: 'center'}}>
                      <Text style={styles.chatText}>City</Text>
                    </View>
                    <View style={{width: "70%"}}>
                      <Picker
                        selectedValue={this.state.city}
                        style={{ width: undefined, borderColor: '#005497', borderWidth: 1 }}
                        onValueChange={(itemValue, itemIndex) =>{
                            this.setState({city: itemValue})
                        }
                        }>
                        <Picker.Item style={{textAlign: 'center'}} label="Select city" value="" />
                          {this.state.cities.map((city,i) =>
                          {
                            return <Picker.Item style={{textAlign: 'center'}} label={city.name} value={city.name} key={i}/>}
                          )}
                      </Picker>
                    </View>
                  </View>
                </View>
                <View style={[styles.blockstyle, {height: 60, padding: 0}]}>
                  <TouchableOpacity style={{}} onPress={() => this.props.navigation.navigate('AddMember')}>
                    <ListItem avatar  onPress={() => this.props.navigation.navigate('AddMember')}>
                      <Left>
                        <Image source={require('../images/icon-add-user.png')} style={{width: 40, height: 40, marginRight: 10}}/>
                      </Left>
                      <Body style={{borderBottomColor: '#ffffff'}} >
                        <Text  style={styles.chatText} note>Select Member</Text>
                      </Body>
                      <Right style={{borderBottomColor: '#ffffff'}}>
                        <Image source={require('../images/icon-blue-forward.png')} style={{width: 15, height: 20, marginTop: 5, marginLeft: 10}}/>
                      </Right>
                    </ListItem>
                  </TouchableOpacity> 
                </View>
                <View style={styles.btnContainer}>
                  <Button block primary  onPress={this.onPressCreate} style={styles.btn}>
                    <Text style={{color: 'white', fontSize: 20}}>Create Group</Text>
                  </Button>
                </View>
            </Form>
          </Content>
          </Container>
        </View>
      </ImageBackground>
    );
  }
 
  
}

const offset = 16;
const shadow ={
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:8,
  shadowOffset:{width:0,height:4}
}
const styles = StyleSheet.create({
  blockstyle: {
    borderColor: '#d6d7da', 
    borderWidth: 1, 
    marginTop: 10,
    borderRadius: 5,
    padding: 10
  },
  title: {
    marginTop: offset,
    marginLeft: offset,
    fontSize: offset
  },
  nameInput: {
    height: offset * 2,
    margin: offset,
    paddingHorizontal: offset,
    borderColor: '#111111',
    borderWidth: 1,
    fontSize: offset
  },
  buttonText: {
    marginLeft: offset,
    fontSize: 42
  },
  btn: {
    width: '60%',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: '#005ca4'
  },
  btnContainer: {
    marginTop: offset,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 10,
  },
  imgBackground: {
    width: deviceWidth,
    height: '25%',
    flex: 1,
    alignItems: 'center'
  },
  startScanBtn: {
    width: deviceWidth - 20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
  progressBar: {
    backgroundColor: 'rgb(3, 154, 229)',
    height: 3,
    shadowColor: '#000',
  },
  chatList:{
    padding: 20,
    marginLeft:5,
    marginRight:5,
    width:deviceWidth-40,
    height:deviceHeight-100,
    marginTop:10,
    marginBottom:40
  },
  chatUserBox:{
  width:deviceWidth-60,
  height:100,
  borderRadius:10,
  backgroundColor:'#ffffff',
  justifyContent:'center',
  marginTop:10,
  marginBottom:10,
  marginLeft:5,
  padding:10
},
btnbadge:{
  width:30,
  height:30,
  justifyContent:'center',
  padding:5,
  borderRadius:15,
  marginTop:5,
},
chatTime:{
  color:'#005ca4',
  fontSize:11,

},
chatName:{
  color:'#282829',
  fontSize:16,
},
chatText:{
  color:'#005ca4',
  fontSize: 16,
},
buttonStyle:{
    alignItems:'center',
    padding:10,
    height:60
  },
  selectedButtonStyle:{
    borderRadius:5,
    alignItems:'center',
    padding:10,
    height:60,
    backgroundColor:'#82ceec'
  },
  mainDisplay:{
    alignItems:'center',
    backgroundColor:'#ffffff',
    borderRadius:10,
    marginTop:10,
    width:deviceWidth-30,
  },
  headerTitle:{
    marginTop:-30,
    // marginRight:deviceWidth-360,
    color:'#ffffff',
    textAlign:'center',
    alignItems: 'center',
    width:200,
  },
  headerTitleText:{
    color:'#ffffff',
    fontSize: 25
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#000000',
    borderRadius: 4,
    color: '#000000',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: '#000000',
    borderRadius: 8,
    color: '#000000',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputBox: {
    width: '100%',
    height: 45,
    backgroundColor: '#1c2635',
    paddingHorizontal: 20,
    fontSize:16,
    color: '#ffffff',
    marginVertical: 10,
    //textAlign: 'center'
  },
  
});
