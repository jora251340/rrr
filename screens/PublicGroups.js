

import React, { Component } from 'react';
import { Platform, SafeAreaView, FlatList,Image,AsyncStorage, TouchableOpacity, Alert, TextInput, Text, View } from 'react-native';
import User from '../User';
import styles from '../constants/styles';
import firebase from 'firebase';
import base64 from 'react-native-base64';
//const emailnew = ''
export default class PublicGroups extends Component {
  
  static navigationOptions =({navigation}) => {
    return {
      title: 'Public Chat Groups',
      headerRight:(
        <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
          <Image source={require('../images/user.png')} style={{width:32,height:32,marginRight:7}}/>
        </TouchableOpacity>
      ),
      
    }
   
  }
  state = {
    groups: [],
  }
  

  componentWillMount() {
    
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email:val,}
        )

        console.log('email in storage',this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email=noQuotes;
        console.log('new user email in func',User.email)

      }
    })

//console.log('email from storage',val);
//User.email=this.state.email;
//console.log('new user email out func',User.email)
//console.log('user name stored in stat',User.email);
//  console.log('user email got from route',UserEmailtest);
   let Grouptype;
    let dbRef = firebase.database().ref('Groups');
    dbRef.on('child_added', (val) => {
     let Group = val.val();
     // console.log('User passed email',User.email)
   // console.log('User predefiend',User.name)
   // console.log(User.email);
      Group.id = val.key;
      Grouptype = Group.Type;
      console.log('chat group id',Group.id)
    //  let decEmail = base64.decode(person.email);
   // console.log(decEmail);
     // console.log('email in db selection',User.email);
     /* if (decEmail === User.email) {
        console.log('Looged in Persons name',person.name);
        User.name = person.name
      //  let username = User.name
     //   this.setState(User.name:username)
        console.log(User.name);
       // this.setState(User.name=User.name);
      } else {*/
        this.setState((prevState) => {
          return {
            groups: [...prevState.groups, Group]
          }
        })
      /*}*/

    })
  }

  renderRow = ({ item }) => {
    return (
      <TouchableOpacity style={{ padding: 10, borderBottomColor: '#ccc', borderBottomWidth: 1 }} onPress={() => this.props.navigation.navigate('GroupChat', item)}>
        <Image source={{uri:item.avatar}} style={{width:40,height:40}} />
        <Text style={{ fontSize: 20 }}>{item.groupname}</Text>
       
      </TouchableOpacity>
    )
  }
  render() {
    return (
      <SafeAreaView>
        <FlatList
          data={this.state.groups}
          renderItem={this.renderRow}
          keyExtractor={(item) => item.id}

        />
          <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateGroup')}>
            <Text style={styles.btnText}>Create new group</Text>
          </TouchableOpacity>
        
      </SafeAreaView>
    );
  }
  
}

