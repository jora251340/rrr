import React, { Component } from 'react';
import { Platform, SafeAreaView, FlatList,Image,AsyncStorage, TouchableOpacity, Alert, TextInput, Text, View,StyleSheet,Dimensions,ImageBackground,StatusBar } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Badge,Spinner } from 'native-base';
import User from '../User';
import firebase from 'firebase';
import base64 from 'react-native-base64';
import {BoxShadow} from 'react-native-shadow';
import LinearGradient from 'react-native-linear-gradient';
//const emailnew = ''
const deviceWidth = Dimensions.get('screen').width;
const shadowOpt = {
	width:deviceWidth-50,
  color:"#000",
  height:85,
	border:1,
	radius:5,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:3,alignItems:'center',backgroundColor:'#ffffff'}
}
export default class HomeScreen extends Component {
  
  static navigationOptions =({navigation}) => {
    return {
      title: 'Chats',
      headerRight:(
        
        <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
          <Image source={require('../images/user.png')} style={{width:32,height:32,marginRight:7}}/>
        </TouchableOpacity>
      ),
      
    }
   
  }
  state = {
    loading:true,
    users: [],
  }
  getInitialState() {
    return({
        state: {
          loading:true,
          users: [],
        }
    })
}

  componentWillMount() {
    
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email:val,}
        )

        console.log('email in storage',this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email=noQuotes;
        console.log('new user email in func',User.email)

      }
    })

//console.log('email from storage',val);
//User.email=this.state.email;
console.log('new user email out func',User.email)
//console.log('user name stored in stat',User.email);
//  console.log('user email got from route',UserEmailtest);
    let dbRef = firebase.database().ref('users');
    dbRef.on('child_added', (val) => {
     let person = val.val();
     // console.log('User passed email',User.email)
   // console.log('User predefiend',User.name)
   // console.log(User.email);
      person.email = val.key;
      let decEmail = base64.decode(person.email);
    console.log(decEmail);
      console.log('email in db selection',User.email);
      if (decEmail === User.email) {
        console.log('Looged in Persons name',person.name);
        User.name = person.name
      //  let username = User.name
     //   this.setState(User.name:username)
        console.log(User.name);
       // this.setState(User.name=User.name);
      } else {
        this.setState((prevState) => {
          return {
            loading:false,
            users: [...prevState.users, person]
          }
        })
      }

    })
  }

  renderRow = ({ item }) => {
    return (
    
      <TouchableOpacity style={[styles.chatUserBox,shadow]} onPress={() => this.props.navigation.navigate('Chat', item)}>
      <ListItem avatar  onPress={() => this.props.navigation.navigate('Chat', item)}>
              <Left>
                <Thumbnail source={{ uri:item.avatar}} />
              </Left>
              <Body style={{borderBottomColor:'#ffffff'}} >
                <Text style={styles.chatName}>{item.name}</Text>
                <Text  style={styles.chatText} note>Doing what you like. .</Text>
              </Body>
              <Right style={{borderBottomColor:'#ffffff'}}>
                <Text style={styles.chatTime} note>3:43 PM</Text>
                <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
  locations={[0,0.6]}
  colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
                   <Text style={{color:'#ffffff'}}>02</Text>
                   </LinearGradient>
         
              </Right>
            </ListItem>
     
       
      </TouchableOpacity>
     
    )
  }
  render() {
    if (this.state.loading) {
      return (
        <ImageBackground style={styles.imgBackground } 
        resizeMode='cover' 
        source={require('../images/header2.png')}>
           <TouchableOpacity style={{marginTop:15,marginLeft:deviceWidth-60,}}>
          <Image source={require('../images/menu.png')} style={{width:30,height:30,}} />
          </TouchableOpacity>
          <TouchableOpacity style={{marginTop:-28,marginRight:deviceWidth-60,}}>
          <Image source={require('../images/notification.png')} style={{width:30,height:30}} />
          </TouchableOpacity>
       
         <StatusBar backgroundColor="#005497" barStyle="light-content"/>
        <View style={{alignItems:'center'}}> 
          <Image source={require('../images/logo.png')} style={{width:150,height:150,marginTop:30}} />
           <Text>Loading Chats...</Text>
          <Spinner />
        
         
        </View>
        </ImageBackground>
        );
    }
    return (
      <ImageBackground style={styles.imgBackground } 
      resizeMode='cover' 
      source={require('../images/header2.png')}>
         <TouchableOpacity style={{marginTop:15,marginLeft:deviceWidth-60,}}>
        <Image source={require('../images/menu.png')} style={{width:30,height:30,}} />
        </TouchableOpacity>
        <TouchableOpacity style={{marginTop:-28,marginRight:deviceWidth-60,}}>
        <Image source={require('../images/notification.png')} style={{width:30,height:30}} />
        </TouchableOpacity>
     
       <StatusBar backgroundColor="#005497" barStyle="light-content"/>
      <View style={{alignItems:'center'}}> 
        <Image source={require('../images/logo.png')} style={{width:150,height:150,marginTop:30}} />
      
        
        <FlatList
          data={this.state.users}
          renderItem={this.renderRow}
          keyExtractor={(item) => item.email}
          style={styles.chatList}
        />
       
      </View>
      </ImageBackground>
    );
  }
  
}

const offset = 16;
const shadow ={
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:8,
  shadowOffset:{width:0,height:4}
}
const styles = StyleSheet.create({
    title: {
        marginTop: offset,
        marginLeft: offset,
        fontSize: offset
    },
    nameInput: {
        height: offset * 2,
        margin: offset,
        paddingHorizontal: offset,
        borderColor: '#111111',
        borderWidth: 1,
        fontSize: offset
    },
    buttonText: {
        marginLeft: offset,
        fontSize: 42
    },
    btn:{
      width:deviceWidth-20,
      justifyContent:'center'
    },
    btnContainer:{
        marginTop:offset,
        alignItems:'center',
       justifyContent:'center',
       flexDirection:'row',
       padding:10,
    },
    imgBackground: {
        width: deviceWidth,
       height: '25%',
        flex: 1,
        alignItems:'center'
  },
  startScanBtn: {
    width:deviceWidth-20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems:'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight:'bold',
    backgroundColor: 'transparent',
  },
  chatList:{
    marginLeft:5,
    marginRight:5,
    width:deviceWidth-40,
  },
  chatUserBox:{
  width:deviceWidth-60,
  height:100,
  borderRadius:10,
  backgroundColor:'#ffffff',
  justifyContent:'center',
  marginTop:10,
  marginBottom:10,
  marginLeft:5,
  padding:10
},
btnbadge:{
  width:30,
  height:30,
  justifyContent:'center',
  padding:5,
  borderRadius:15,
  marginTop:5,
},
chatTime:{
  color:'#005ca4',
  fontSize:11,

},
chatName:{
  color:'#282829',
  fontSize:16,
},
chatText:{
  color:'#9e9e9e',
  fontSize:11,
}
});
