import React, { Component } from 'react';
import { Platform, Dimensions, SafeAreaView, FlatList, Image, AsyncStorage, TouchableOpacity, Alert, TextInput,  View, StyleSheet } from 'react-native';
import { Container, Header, Content, Text, Button, Toast, ListItem, Left, Right, Body, Thumbnail } from "native-base";
import User from '../User';
import firebase from 'firebase';
import base64 from 'react-native-base64';
import LinearGradient from 'react-native-linear-gradient';
//const emailnew = ''
let membermail;
let memberavatar;
let membername;
const deviceWidth = Dimensions.get('screen').width;
export default class AddGroupMembers extends Component {  
  static navigationOptions = ({ navigation }) => {
    return {
      // title: navigation.getParam('name', null),
      headerStyle: { backgroundColor: '#004E8C', height: 70 },
      // headerTitleStyle: { color: '#ffffff', padding: 20, fontSize: 22, fontWeight: '300' },
      // headerTintColor: '#ffffff',
      headerBackground: (
        <LinearGradient
          colors={['#004E8C', '#007FE3']}
          style={{ flex: 1 }}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
        />
      ),
      headerLeft: (
        <TouchableOpacity onPress={()=> navigation.goBack()}>
          <View style={styles.chatBackBtn} >
            <Image source={require('../images/btn_back_2.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 10}}/>
            <View>
              <View>
                <Text style={{fontSize: 25, fontWeight: '300', color: '#ffffff'}}>Select Members</Text>
              </View>
              <View>
                {/* <Text style={{fontSize: 14, color: '#ffffff'}}>{navigation.getParam('num_members', null)}</Text> */}
                <Text style={{fontSize: 14, color: '#ffffff'}}>9 Members</Text>
              </View>
            </View>    
          </View>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity>
          <Image source={require('../images/icon-search.png')} style={{width: 25, height: 25, marginRight: 20}}/>                 
        </TouchableOpacity>
      )            
    }
  }
  constructor(props) {
    super(props);
    this.state = {
        users: [],
        groupid:props.navigation.getParam('GroupId'),
        showToast: false
    }
    this.addMember = this.addMember.bind(this);
  }

  addMember(membermail,membername,memberavatar) {
    let GroupId = this.state.groupid;
    let decmembermail = base64.decode(membermail);
    
    let dbRef = firebase.database().ref('Groups/'+ GroupId +'/GroupMembers/'+membermail);
      dbRef.once('value', (val) => {
      let person = val.val();
        if(person!= null) {
          person.email = val.key;
          // alert(person.email);
          let decEmail = base64.decode(person.email);    
            alert('Member Alreaady Exits in the group');
        } else {
          GroupId = this.state.groupid;
          const Groupmember = {
              membername: membername,
              memberemail:membermail,
              memberavatar:memberavatar
          };
          //  await AsyncStorage.setItem('userEmail', this.state.email);
          // User.email = this.state.email;
          // firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
          // firebase.createAccount(user);
          firebase.database().ref('Groups/' + GroupId + '/GroupMembers/'+membermail).set({ membername: Groupmember.membername, memberemail:base64.decode(Groupmember.memberemail), memberavatar:Groupmember.memberavatar })
          alert('Member Added Successfully')
        }     
      })
    }

  componentWillMount() {    
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email:val,})
        console.log('email in storage',this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email=noQuotes;
        console.log('new user email in func',User.email)
      }
    })

    //console.log('email from storage',val);
    //User.email=this.state.email;
    //console.log('new user email out func',User.email)
    //console.log('user name stored in stat',User.email);
    //  console.log('user email got from route',UserEmailtest);
    let dbRef = firebase.database().ref('users');
    dbRef.on('child_added', (val) => {
      let person = val.val();
      // console.log('User passed email',User.email)
      // console.log('User predefiend',User.name)
      // console.log(User.email);
      person.email = val.key;
      let decEmail = base64.decode(person.email);
      console.log(decEmail);
      // console.log('email in db selection',User.email);
      this.setState((prevState) => {
        return {
          users: [...prevState.users, person]
        }
      })
    })
  }

  renderRow = ({ item }) => {
    return (    
      <TouchableOpacity style={[styles.chatUserBox, shadow]} onPress={()=>this.addMember(item.email,item.name,item.avatar)}>
        <ListItem avatar onPress={()=>this.addMember(item.email,item.name,item.avatar)}>
          <Left>
            <Thumbnail style={{marginBottom: 12}} source={{ uri: item.avatar }} />
          </Left>
          <Body style={{borderBottomColor: '#ffffff'}} >
            <Text style={styles.chatName}>{item.name}</Text>
          </Body>
          <Right>
          </Right>
        </ListItem>
      </TouchableOpacity>  
    )
  }
  render() {
    return (
      <SafeAreaView>
        <TouchableOpacity style={[styles.chatUserBox, shadow]} onPress={() => this.props.navigation.navigate('CreateGroup')}>
          <ListItem avatar onPress={() => this.props.navigation.navigate('CreateGroup')}>
            <Left>
              <Thumbnail style={{marginBottom: 12}} source={require('../images/icon-create-group1.png')} />
            </Left>
            <Body style={{borderBottomColor: '#ffffff'}} >
              <Text style={styles.chatName}>Create Group</Text>
            </Body>
            <Right>
            </Right>
          </ListItem>
        </TouchableOpacity>  
        <FlatList
          data={this.state.users}
          renderItem={this.renderRow}
          keyExtractor={(item) => item.email}
        />
      </SafeAreaView>
    );
  }
  
}

const shadow = {
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:15,
  shadowOffset:{width:0,height:10}
}

const styles = StyleSheet.create({
  chatName:{
    color:'#282829',
    fontSize:16,
  },
  chatBackBtn:{
    flex:1,
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center',
    marginLeft:5,
    padding:5, 
  },
  chatUserBox: {
    width: deviceWidth-60,
    height: 80,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 30,
    padding: 10
  }
})