import React, { Component } from 'react';
import { SafeAreaView, Text, View, Dimensions, TextInput, TouchableOpacity, KeyboardAvoidingView,Image,StyleSheet } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Badge } from 'native-base';
//import styles from '../constants/styles';
import relativeDate from 'relative-date';
import LinearGradient from 'react-native-linear-gradient';
import User from '../User';
import firebase from 'firebase';
//import firebase from 'react-native-firebase';
import { FlatList } from 'react-native-gesture-handler';
import base64 from 'react-native-base64';
import { GiftedChat } from "react-native-gifted-chat";
import firebaseSDK from '../firebaseSDK';

const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;

export default class GroupChatSingle extends Component {
  
    _isMounted = false;

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('groupname', null),
            headerStyle: { backgroundColor:'#005497', height: 70},
            headerTitleStyle: { color: '#ffffff',padding:20,fontSize:22,fontWeight:'300', },
            headerTintColor:'#ffffff',
            headerBackground: (
              <LinearGradient
                colors={['#004E8C', '#007FE3']}
                style={{ flex: 1 }}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
              />
            ),
            headerLeft:(
                <TouchableOpacity onPress={()=>navigation.goBack()}>
                <View style={styles.chatBackBtn} >
                  <Image source={require('../images/btn_back_2.png')} style={{width: 20, height: 20, marginRight: 10, marginLeft: 10}}/>
                  <Thumbnail source={{uri:navigation.getParam('avatar'),}} style={{width:40,height:40,marginRight:7}}/>                  
                </View>
                </TouchableOpacity>
              ),
              headerRight: (
                <TouchableOpacity onPress={()=>navigation.navigate('GroupOptions',{
                  GroupId:navigation.getParam('id'),
                  Groupname:navigation.getParam('groupname'),
                })}>
                  <Image source={require('../images/options.png')} style={{width: 20, height: 20, marginRight: 10}}/>                 
                </TouchableOpacity>
              ) 
          
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            Groupinfo: {
                groupname: props.navigation.getParam('groupname'),
                groupid:props.navigation.getParam('id'),
            },
            textMessage: '',
            groupmessageList: [],
            user:{
                name: User.name,
                email: User.email,
               // avatar: User.avatar,
               // id: firebaseSDK.uid,
               // _id: firebaseSDK.uid  
            }
        }

    }

    get user() {
      
        return {
            name: User.name,
            email: User.email,
          //  avatar: User.avatar,
           // id: firebaseSDK.uid,
           // _id: firebaseSDK.uid
        };
    }

    
    componentWillMount() {
        this._isMounted = true;
        firebase.database().ref('Groups').child(this.state.Groupinfo.groupid).child('groupmessages')
            .on('child_added', (value) => {
                if (this._isMounted) {
                this.setState((prevState) => {
                    return {
                        groupmessageList:[...prevState.groupmessageList,value.val()]
                    }
                })
            }
            })
        
    }

    handleChange = key => val => {
        this.setState({ [key]: val })
    }
 convertTime = (time) => {
     
        let result = relativeDate(new Date(time));
      
        return result;
    }
    sendMessage = async () => {
        if (this.state.textMessage.length > 0) {
            let msgId = firebase.database().ref('Groups').child(this.state.Groupinfo.groupid).child('groupmessages').push().key;
            let updates = {};
            let message = {
                message: this.state.textMessage,
                createdAt: firebase.database.ServerValue.TIMESTAMP,
                from: User.email,
                user:this.state.user
            }
            updates['Groups/' + this.state.Groupinfo.groupid + '/' + 'groupmessages' + '/' + msgId] = message;
           // updates['messages/' + base64.encode(this.state.person.email) + '/' + base64.encode(User.email) + '/' + msgId] = message;
            firebase.database().ref().update(updates);
            if (this._isMounted) {
            this.setState({ textMessage: '' });
            }
        }
    }


    renderRow =({item})=>{
        return (
            <View>
            {
                item.from===User.email ? 
                <View style={{width:'100%'}}>
                   
                     <LinearGradient colors={['#004E8C', '#007FE3']} start={{x: 1.0, y: 1.0}} end={{x: 0.0, y: 1.0}}
                                      style={{flexDirection:'row',
                                      width:'60%',
                                      alignSelf:'flex-end',
                                     // backgroundColor:item.from===User.email ? '#00897b':'#7cb342',
                                      borderRadius: 8,
                                      padding:8,
                                      marginBottom:10} }>
                                    <Text style={{color:'#fff',padding:7,fontSize:16}}>
         {item.message}
         </Text>
         <Text style={{color:'#ccc',padding:3,fontSize:12}} >{this.convertTime(item.createdAt)}</Text>
                                    </LinearGradient> 
                                    </View>
                     :<View style={{width:'100%',flexDirection:'row'}}>
                     <View style={{width:'15%'}}>
                          <Image  style={styles.avatarStyle} source={{uri:"https://firebasestorage.googleapis.com/v0/b/chatapp-f57d9.appspot.com/o/groupavatars%2Favatar.png?alt=media&token=2060c383-edc4-484b-a593-9d9344685bc4"}}/>
                          <Text style={{color:'#ccc',padding:3,fontSize:12}} >{item.user.name}</Text>
                          </View>
                        <LinearGradient colors={['#E3E5EF', '#E3E5EF']} start={{x: 1.0, y: 1.0}} end={{x: 0.0, y: 1.0}}
                                    style={{
                                        flexDirection:'row',
                                        width:'60%',
                                        alignSelf:'flex-start',
                                       // backgroundColor:item.from===User.email ? '#00897b':'#7cb342',
                                       borderRadius: 8,
                                       padding:8,
                                        marginBottom:10
                                         } }>
                                          <Text style={styles.userText}>
         {item.message}
         </Text>
         <Text style={{color:'#ccc',padding:3,fontSize:12}} >{this.convertTime(item.createdAt)}</Text>
                                </LinearGradient>
                                </View>
                       
            }
       </View>

        )
    }
   
    render() {
        let { height, width } = Dimensions.get('window');
        console.log(this.state.messageList)
        return (
          <SafeAreaView>
            <View style={{padding:10,height:height-160}}>        
              <FlatList 
                style={{padding:10}}
                data={this.state.groupmessageList}
                renderItem={this.renderRow}
                keyExtractor={(item,index) =>index.toString()}
              />
            </View>
            <View style={[{height: 70, borderTopColor: '#f4f4f4', borderTopWidth: 2}]}>
              <KeyboardAvoidingView behavior="margin"> 
                <View style={[styles.keyboard]}>
                  <View style={{ width: '70%', marginLeft: 10, flexDirection: "row" }}>
                    <View style={{}}>
                      <Image source={require('../images/icon-plus-btn.png')} style={{width: 40, height: 40, marginRight: 10}}/>
                    </View>
                    <View style={{}}>
                      <TextInput
                        style={styles.input}
                        value={this.state.textMessage}
                        placeholder="Write your message"
                        onChangeText={this.handleChange('textMessage')} />
                    </View>
                  </View>
                  <View style={{ width: '30%' }}>
                    <TouchableOpacity onPress={this.sendMessage}>
                      <Image source={require('../images/btn_sendmsg.png')} style={{height: "100%", width: "100%"}}/>                 
                    </TouchableOpacity>
                  </View>
                </View>
              </KeyboardAvoidingView>
            </View>
       </SafeAreaView>
           
       
        )
    }
    componentWillUnmount() {
        this._isMounted = false;
      }
    
    
}

const offset = 16;
const shadow ={
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:8,
  shadowOffset:{width:0,height:4}
}
const styles = StyleSheet.create({
    keyboard: {
      width: '100%', 
      flexDirection: "row", 
      alignItems: 'center', 
      marginHorizontal: 0, 
      height: 70,
    }, 
    title: {
        marginTop: offset,
        marginLeft: offset,
        fontSize: offset
    },
    nameInput: {
        height: offset * 2,
        margin: offset,
        paddingHorizontal: offset,
        borderColor: '#111111',
        borderWidth: 1,
        fontSize: offset
    },
    buttonText: {
        marginLeft: offset,
        fontSize: 42
    },
    btn:{
      width:deviceWidth-20,
      justifyContent:'center'
    },
    btnContainer:{
        marginTop:offset,
        alignItems:'center',
       justifyContent:'center',
       flexDirection:'row',
       padding:10,
    },
    imgBackground: {
        width: deviceWidth,
       height: '25%',
        flex: 1,
        alignItems:'center'
  },
  startScanBtn: {
    width:deviceWidth-20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems:'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight:'bold',
    backgroundColor: 'transparent',
  },
  chatList:{
    marginLeft:5,
    marginRight:5,
    width:deviceWidth-40,
  },
  chatUserBox:{
  width:deviceWidth-60,
  height:100,
  borderRadius:10,
  backgroundColor:'#ffffff',
  justifyContent:'center',
  marginTop:10,
  marginBottom:10,
  marginLeft:5,
  padding:10
},
btnbadge:{
  width:30,
  height:30,
  justifyContent:'center',
  padding:5,
  borderRadius:15,
  marginTop:5,
},
chatTime:{
  color:'#005ca4',
  fontSize:11,

},
chatName:{
  color:'#282829',
  fontSize:16,
},
chatText:{
  color:'#9e9e9e',
  fontSize:11,
},
chatBackBtn:{
  flex:1,
  flexDirection:'row',
  justifyContent:'space-around',
  alignItems:'center',
  marginLeft:5,
  padding:5, 
},
mineMessage: {
    borderRadius: 8,
    padding:8,
  },
  userText: {
    color: '#7B869C',
    fontSize: 16,
    fontWeight: '300',
    flex: 1,
  },
  mineText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    flex: 1,
  },
  avatarStyle: {
    borderRadius: 30,
    marginRight: 10,
    width: 40,
    height: 40,
    marginBottom:5,

  }
});