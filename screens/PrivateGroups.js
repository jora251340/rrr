import React, { Component } from 'react';
import { Platform, SafeAreaView, FlatList,Image,AsyncStorage, TouchableOpacity, Alert, TextInput, Text, View,StyleSheet,Dimensions,ImageBackground,StatusBar ,TouchableHighlight} from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Badge,Spinner,Segment,Button } from 'native-base';
import User from '../User';
import firebase from 'firebase';
import base64 from 'react-native-base64';
import {BoxShadow} from 'react-native-shadow';
import LinearGradient from 'react-native-linear-gradient';
//const emailnew = ''
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const shadowOpt = {
	width:deviceWidth-50,
  color:"#000",
  height:85,
	border:1,
	radius:5,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:3,alignItems:'center',backgroundColor:'#ffffff'}
}
export default class Groups extends Component {
  
  static navigationOptions =({navigation}) => {
    return {
        header:null,      
    }   
  }
  constructor(props) {
    super(props);
    this.state = {
        countryname:props.navigation.getParam('countryname'),
        cityname:props.navigation.getParam('name'),
          loading:true,
          loadingpublic:true,
          loadingprivate:true,
          users: [],
          activePage: 1,
          groups: [],
          publicgroups: [],
       
    }

}
  
selectComponent = (activePage) => () => this.setState({activePage})
  getInitialState() {
    return({
        state: {
        countryname:props.navigation.getParam('countryname'),
        cityname:props.navigation.getParam('name'),
          loading:true,
          loadingpublic:true,
          loadingprivate:true,
          users: [],
          groups: [],
          publicgroups: [],
        }
    })
}

  componentWillMount() {
    
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email:val,}
        )

        console.log('email in storage',this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email=noQuotes;
        console.log('new user email in func',User.email)

      }
    })

//console.log('email from storage',val);
//User.email=this.state.email;
console.log('new user email out func',User.email)
//console.log('user name stored in stat',User.email);
//  console.log('user email got from route',UserEmailtest);

   let dbRef2 = firebase.database().ref('Groups');
dbRef2.on('child_added', (val) => {
 let Group = val.val();
 
  Group.id = val.key;
 
  console.log('chat group id',Group.id);
  let encmemberemail=base64.encode(User.email);
        let dbRef2 = firebase.database().ref('Groups/'+ Group.id +'/GroupMembers/'+encmemberemail);
        dbRef2.once('value', (val) => {
         let person = val.val();
         if(person!= null){
          person.email = val.key;
          let selectedcityname =this.state.cityname;
          let GroupType = "Private";
          if(Group.Grouptype===GroupType){
           // alert(person.email);
           this.setState((prevState) => {
            return {
             loadingprivate:false,
              groups: [...prevState.groups, Group]
            }
          })
        } 
         }else {

         }
        })

})
let dbRef3 = firebase.database().ref('Groups');
dbRef3.on('child_added', (val) => {
 let PublicGroup = val.val();
 
  PublicGroup.id = val.key;
  let selectedcityname =this.state.cityname;
  let GroupType = "Public";
  //console.log('chat group id',Group.id);
  let encmemberemail=base64.encode(User.email);
            
             if(PublicGroup.Grouptype===GroupType){
                 //alert(PublicGroup.id)
                this.setState((prevState) => {
                    return {
                        loadingpublic:false,
                      publicgroups: [...prevState.publicgroups, PublicGroup]
                    }
                  }) 

             }
        
           // alert(person.email);
          
        
      

})
   
  }

 
  renderRowGroup = ({ item }) => {
    return (
    
      <TouchableOpacity style={[styles.chatUserBox,shadow]} onPress={() => this.props.navigation.navigate('GroupChat', item)}>
      <ListItem avatar  onPress={() => this.props.navigation.navigate('GroupChat', item)}>
              <Left>
                <Thumbnail source={{ uri:item.avatar}} />
              </Left>
              <Body style={{borderBottomColor:'#ffffff'}} >
                <Text style={styles.chatName}>{item.groupname}</Text>
                <Text  style={styles.chatText} note>Lorem ipsum Sit amet</Text>
              </Body>
              <Right style={{borderBottomColor:'#ffffff'}}>
                <Text style={styles.chatTime} note>3:43 PM</Text>
                <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
  locations={[0,0.6]}
  colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
                   <Text style={{color:'#ffffff'}}>02</Text>
                   </LinearGradient>
         
              </Right>
            </ListItem>
     
       
      </TouchableOpacity>
     
    )
  }
  renderRowPublicGroup = ({ item }) => {
    return (
    
      <TouchableOpacity style={[styles.chatUserBox,shadow]} onPress={() => this.props.navigation.navigate('GroupChat', item)}>
      <ListItem avatar  onPress={() => this.props.navigation.navigate('GroupChat', item)}>
              <Left>
                <Thumbnail source={{ uri:item.avatar}} />
              </Left>
              <Body style={{borderBottomColor:'#ffffff'}} >
                <Text style={styles.chatName}>{item.groupname}</Text>
                <Text  style={styles.chatText} note>{item.GroupDescription}</Text>
              </Body>
              <Right style={{borderBottomColor:'#ffffff'}}>
                <Text style={styles.chatTime} note>3:43 PM</Text>
                <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
  locations={[0,0.6]}
  colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
                   <Text style={{color:'#ffffff'}}>02</Text>
                   </LinearGradient>
         
              </Right>
            </ListItem>
     
       
      </TouchableOpacity>
     
    )
  }
  _renderTitle = () => {
   
    if(this.state.activePage === 1)
    return (
        <Text style={styles.headerTitleText}>Public Groups</Text>
    ) 
    else if(this.state.activePage === 2)
    return (
        <Text style={styles.headerTitleText}>My Groups</Text>
    )
  }
  _renderComponent = () => {
  if(this.state.activePage === 1)
  if (this.state.loadingpublic) {
    return (
        
           <View>
           <Text>Loading Group Chats...</Text>
          <Spinner />
         
             <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateGroup')} style={styles.addGroupempty}  >
          
                       <Image  source={require('../images/add-group.png')}  style={{width:30,height:30}}/>
                     
          </TouchableOpacity>
          {
              this.state.publicgroups.length > 0 ?<Text></Text>:<Text>No public groups available in this city</Text>
          }
          </View>
        
         
       
        );
}else{
    return (
        
        
          <View>
          <FlatList
            data={this.state.publicgroups}
            renderItem={this.renderRowPublicGroup}
            keyExtractor={(item) => item.id}
            style={styles.chatList}
          />
        
          <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateGroup')} style={styles.addGroup}  >
          
                       <Image  source={require('../images/add-group.png')}  style={{width:30,height:30}}/>
                     
          </TouchableOpacity>
          
         </View>
       
      );

}
     //... Your Component 1 to display
  else if(this.state.activePage === 2)
  if (this.state.loadingprivate) {
    return (
        
           <View>
           <Text>Loading Group Chats...</Text>
          <Spinner />
          <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateGroup')} style={styles.addGroupempty}  >
          
          <Image  source={require('../images/add-group.png')}  style={{width:30,height:30}}/>
        
</TouchableOpacity>
{
              this.state.groups.length > 0 ?<Text></Text>:<Text>You have No Private groups </Text>
          }
          </View>
        
         
       
        );
}else{
    return (
        
        
          <View>
          <FlatList
            data={this.state.groups}
            renderItem={this.renderRowGroup}
            keyExtractor={(item) => item.id}
            style={styles.chatList}
          />
          
          <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateGroup')} style={styles.addGroup}  >
          
                       <Image  source={require('../images/add-group.png')}  style={{width:30,height:30}}/>
                     
          </TouchableOpacity>
          
         </View>
       
      );

}
 
 
}
  render() {
    return(
        <ImageBackground style={styles.imgBackground } 
        resizeMode='cover' 
        source={require('../images/header2.png')}>
           <TouchableOpacity style={{marginTop:15,marginLeft:deviceWidth-60,}}>
          <Image source={require('../images/menu.png')} style={{width:30,height:30,}} />
          </TouchableOpacity>
       
          <TouchableOpacity style={{marginTop:-28,marginRight:deviceWidth-60,}}>
          <Image source={require('../images/notification.png')} style={{width:30,height:30}} />
          </TouchableOpacity>
          <View style={styles.headerTitle}>{this._renderTitle()}</View>
       
         <StatusBar backgroundColor="#005497" barStyle="light-content"/>
        <View style={[styles.mainDisplay,shadow]}> 
          
    <Segment  style={{backgroundColor: '#ffffff',height:80,justifyContent:'space-around'}}>
    <Button first active={this.state.activePage === 1}  style={(this.state.activePage === 1)? styles.selectedButtonStyle:styles.buttonStyle} onPress={this.selectComponent(1)}  >
    <Image  source={require('../images/publicg.png')} style={{width:30,height:30}} />
    </Button>
    <Button active={this.state.activePage === 2 }  onPress={this.selectComponent(2)}  style={(this.state.activePage === 2)? styles.selectedButtonStyle:styles.buttonStyle} >
    <Image  source={require('../images/privateg.png')} style={{width:30,height:30}} />
    </Button >
   
    
  </Segment>
 
  
  {this._renderComponent()}
  
  </View>
        </ImageBackground>)
   
  }
  
}

const offset = 16;
const shadow ={
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:8,
  shadowOffset:{width:0,height:4}
}
const styles = StyleSheet.create({
    title: {
        marginTop: offset,
        marginLeft: offset,
        fontSize: offset
    },
    nameInput: {
        height: offset * 2,
        margin: offset,
        paddingHorizontal: offset,
        borderColor: '#111111',
        borderWidth: 1,
        fontSize: offset
    },
    buttonText: {
        marginLeft: offset,
        fontSize: 42
    },
    btn:{
      width:deviceWidth-20,
      justifyContent:'center'
    },
    btnContainer:{
        marginTop:offset,
        alignItems:'center',
       justifyContent:'center',
       flexDirection:'row',
       padding:10,
    },
    imgBackground: {
        width: deviceWidth,
       height: '25%',
        flex: 1,
        alignItems:'center'
  },
  startScanBtn: {
    width:deviceWidth-20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems:'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight:'bold',
    backgroundColor: 'transparent',
  },
  chatList:{
    marginLeft:5,
    marginRight:5,
    width:deviceWidth-40,
    height:deviceHeight-200,
    marginTop:10,
    marginBottom:40
  },
  chatUserBox:{
  width:deviceWidth-60,
  height:100,
  borderRadius:10,
  backgroundColor:'#ffffff',
  justifyContent:'center',
  marginTop:10,
  marginBottom:10,
  marginLeft:5,
  padding:10
},
btnbadge:{
  width:30,
  height:30,
  justifyContent:'center',
  padding:5,
  borderRadius:15,
  marginTop:5,
},
chatTime:{
  color:'#005ca4',
  fontSize:11,

},
chatName:{
  color:'#282829',
  fontSize:16,
},
chatText:{
  color:'#9e9e9e',
  fontSize:11,
},
buttonStyle:{
    alignItems:'center',
    padding:10,
    height:60
  },
  selectedButtonStyle:{
    borderRadius:5,
    alignItems:'center',
    padding:10,
    height:60,
    backgroundColor:'#82ceec'
  },
  mainDisplay:{
    alignItems:'center',
    backgroundColor:'#ffffff',
    borderRadius:10,
    marginTop:20,
    width:deviceWidth-30,
  },
  headerTitle:{
    marginTop:-28,
    marginRight:deviceWidth-360,
    color:'#ffffff',
    textAlign:'center',
    alignItems:'center',
    width:200,
  },
  headerTitleText:{
    color:'#ffffff',
    fontSize:16,
    textAlign:'center',
    fontWeight:'bold'
  },
  addGroup:{
    position:'absolute',
   zIndex:11,
    right:20,
    bottom:220,
    backgroundColor:'#005da5',
    width:60,
    height:60,
    borderRadius:50,
    alignItems:'center',
    justifyContent:'center',
    elevation:8,
  },
  addGroupempty:{
    position:'absolute',
   zIndex:11,
    right:10,
    bottom:20,
    backgroundColor:'#005da5',
    width:60,
    height:60,
    borderRadius:50,
    alignItems:'center',
    justifyContent:'center',
    elevation:8,
  },
});
