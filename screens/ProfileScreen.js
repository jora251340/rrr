import React, { Component } from 'react';
import { Platform, SafeAreaView, FlatList,Image,AsyncStorage, TouchableOpacity, Alert, TextInput, Text, View,StyleSheet,Dimensions,ImageBackground,StatusBar,NetInfo } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail,Badge,Spinner ,Form,Item,Button,Label,Picker,Textarea,Input,Icon} from 'native-base';
import User from '../User';
import firebase from 'firebase';
import base64 from 'react-native-base64';
import ImagePicker from 'react-native-image-picker';
import {BoxShadow} from 'react-native-shadow';
import LinearGradient from 'react-native-linear-gradient';
import OfflineNotice from './OfflineNotice';
import RNFetchBlob from 'rn-fetch-blob'
import { ScrollView } from 'react-native-gesture-handler';

const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;

export default class ProfileScreen extends React.Component {
    _isMounted = false;
    static navigationOptions =({navigation}) => {
        return {
            header: null,      
        }   
    }
    state = {
        changeName: 0,
        email: User.email,
        username: User.name,
        useravatar: User.avatar,
        country:undefined,
        description:'',
        response:null,
        progress:null,
        country:'',
        city: '',
        uploading:'Upload User Avatar ',
        name: User.name,
        changeTravel: 0,
        images: null,
    }
   
    componentWillMount() {
        this._isMounted = true;
        firebase.database().ref('/users/' + base64.encode(User.email)).once('value').then(function (snapshot) {
            var username = (snapshot.val() && snapshot.val().name);
            User.name = username;
            User.avatar = snapshot.val().avatar;
            // alert(this.state.username)
            console.log('username from login db',User.name)
           // console.log('Username log from login',User.name);
            if(this._isMounted){
              this.setState({username: username})  
            }
        });
        this.getPlacesVisited();
    }

    onChangeTextUsername = username => this.setState({ username: username })  ;
    onChangeDescription = description => this.setState({ description: description });

    changeName = async () => {
        if (this.state.name.length < 3) {
            Alert.alert('Error', 'Please Enter valid name');
        } else if (User.name !== this.state.name) {
            firebase.database().ref('users').child(User.email).set({ name: this.state.name })
            User.name = this.state.name;
            Alert.alert('Sucess', 'Name changed successfully');
        }
    }

    handleChange = key => val => {
        this.setState({ [key]: val })
    }

    onImageUpload = async (flag) => {
        try {
            var options = {
            title: 'Select Avatar',
            customButtons: [
                {name: 'fb', title: 'Choose Photo from Facebook'},
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        await  ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                this.uploadImage(response.uri,response.type)
                    .then(url => { 
                        if (flag == 0) {
                            this.setState({groupavatar: url})
                            this.setState({ avatar: url,uploading:'Uploaded' })
                        } else {
                            this.images.push('url')
                        }                        
                    })
                    .catch(error => console.log(error))
            }
        });
        } catch (err) {
            console.log('onImageUpload error:' + err.message);
            alert('Upload image error:' + err.message);
        }
    };

    getPlacesVisited() {
        let encEmail = base64.encode(this.state.email);
        firebase.database().ref('/places/encEmail').once('value', (snapshot) => {
            // alert(snapshot.val());
            if (snapshot.val() == undefined)
                return;
            const places = Object.values(snapshot.val());
            this.setState({images :places.filter(item => (item.useremail == this.state.email) ) })
        })
    }

    savePlacesVisited(path) {
        let encEmail = base64.encode(this.state.email);
        firebase.database().ref('places/encEmail').set({ email: this.state.email, places: path })
        Alert.alert('Success', 'Upload image successfully');
    }

    changeName = async () => {
        if (this.state.name.length < 3) {
            Alert.alert('Error', 'Please Enter valid name');
        } else if (User.name !== this.state.name) {
            firebase.database().ref('users').child(User.email).set({ name: this.state.name })
            User.name = this.state.name;
            Alert.alert('Sucess', 'Name changed successfully');
        }
    }
    
    _logOut = async () => {
       // await AsyncStorage.setItem('userEmail',null)
        await AsyncStorage.clear();
        this.setState(User.email=null)
        this.setState(User.name=null)
        this.props.navigation.navigate('Auth');
    }
    render() {
        return (
            <ImageBackground style={styles.imgBackground } 
                      resizeMode='cover' 
                      source={require('../images/header2.png')}>
                <OfflineNotice />
                <TouchableOpacity style={{marginTop:20,marginRight:deviceWidth-60,}} onPress={()=>this.props.navigation.goBack()}>
                    <Image source={require('../images/btn_back_2.png')} style={{width: 15, height: 20, marginRight: 10, marginLeft: 20}}/>
                </TouchableOpacity>
                <View style={styles.headerTitle}>
                    <Text style={styles.headerTitleText}>Profile</Text>
                </View>
                <StatusBar backgroundColor="#005497" barStyle="light-content"/>
                <View > 
                    <View style={[styles.mainDisplay,shadow, {marginBottom: 30, height: 250}]}>
                        <View style={{flexDirection: 'row', width: "100%", top: 200}}>
                                <View style={[styles.btnContainer_left]}>
                                    <TouchableOpacity>
                                        <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                                                        locations={[0,0.6]}
                                                        colors={['#005da5', '#007ddf']} style={styles.startScanBtn}>
                                            <Text style={styles.buttonText}>
                                                Follow
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.btnContainer_right}>
                                    <TouchableOpacity>
                                        <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                                                        locations={[0,0.6]}
                                                        colors={['#005da5', '#007ddf']} style={styles.startScanBtn}>
                                            <Text style={styles.buttonText}>
                                                Message
                                            </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        <View style={[styles.chatList, {top: -80}]}>
                            <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity onPress={()=>{this.onImageUpload(0); this.forceUpdate();}} style={{flexDirection:'row', marginRight: 10}}>
                                    <Thumbnail source={{uri: this.state.useravatar}} style={{width: 60, height: 60, marginRight: 5}}/> 
                                    <Image source={require('../images/camera.png')} style={{width: 20, height: 20, marginTop: 40, marginLeft: -20}}/>
                                </TouchableOpacity>
                                {
                                    this.state.changeName == 0 ?
                                    <TouchableOpacity onPress={() => {this.state.changeName = 1; this.forceUpdate();}}>
                                        <Label style={[styles.chatText, {marginTop: 20}]}>
                                            <Text style={{fontSize: 20, fontWeight: '300', color: '#005ca4'}}>{this.state.username}</Text>
                                            <Image source={require('../images/icon-edit.png')} style={{width: 10, height:Platform.OS === 'ios' ? 15:10, marginLeft: 15, marginTop: Platform.OS === 'ios'? 0 : -5}} />
                                        </Label>
                                    </TouchableOpacity>
                                    :
                                    <Input
                                        placeholder="Your Name"
                                        onChangeText={this.onChangeTextUsername}
                                        value={this.state.username}
                                    />
                                }
                            </View>
                            <View style={styles.blockstyle}>
                                <Label style={{fontSize: 20, fontWeight: '300', color: '#005ca4'}}>About
                                    <Image source={require('../images/icon-edit.png')} style={{width: 10, height: Platform.OS === 'ios' ? 15:10, marginLeft: Platform.OS === 'ios'? 10:5, marginTop: Platform.OS === 'ios' ? 0 : -5}} />
                                </Label>
                                <Textarea style={{height: 70}} rowSpan={5} placeholder="Description"  onChangeText={this.onChangeDescription}
                                    value={this.state.description} />
                            </View>
                        </View>
                    </View>
                    <View style={[styles.mainDisplay, shadow, ]}>
                        <View style={[styles.chatList, {marginBottom: 10, marginTop: 0}]}>
                            <TouchableOpacity onPress={() => {this.state.changeTravel = 1; this.forceUpdate();}}>
                                <Label style={[styles.chatText, {marginBottom: 20}]}>
                                    <Text style={{fontSize: 20, fontWeight: '300', color: '#005ca4'}}>Next Travels</Text>
                                    <Image source={require('../images/icon-edit.png')} style={{width: 10, height: Platform.OS === 'ios' ? 15:10, marginLeft: Platform.OS === 'ios'? 10:5, marginTop: Platform.OS === 'ios' ? 0 : -5}}/>
                                </Label>
                            </TouchableOpacity>
                            <View style={{justifyContent: 'center', flexDirection: 'row', width: "100%", marginBottom: 10, paddingLeft: 0, paddingRight: 0}}>
                                <View style={{width: "50%", justifyContent: 'center', flexDirection: 'row'}}>
                                    <Text style={styles.chatText}>Date: </Text>
                                    <Text style={styles.contentText}>14/06/2019</Text>
                                </View>
                                <View style={{width: "50%", flexDirection: 'row'}}>
                                    <Text style={styles.chatText}>Where: </Text>
                                    <Text style={styles.contentText}>UAS</Text>
                                </View>
                            </View>
                            <View style={{justifyContent: 'center', flexDirection: 'row', width: "100%"}}>
                                <View style={{width: "50%", justifyContent: 'center', flexDirection: 'row'}}>
                                    <Text style={styles.chatText}>Date: </Text>
                                    <Text style={styles.contentText}>14/06/2019</Text>
                                </View>
                                <View style={{width: "50%", flexDirection: 'row'}}>
                                    <Text style={styles.chatText}>Where: </Text>
                                    <Text style={styles.contentText}>GERMANY</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{padding: 20, height: 180}}>
                        <View>
                            <Text style={{fontSize: 20, fontWeight: '300', color: '#005ca4'}}>Places Visited</Text>
                        </View>
                        <View>
                            <ScrollView horizontal>
                                { 
                                    (this.state.images != null) ?
                                        this.state.images.forEach(image => {
                                            <View style={styles.placephotostyle}>
                                                <Image source={{uri: image}} style={{width: '100%', height: '100%'}}/>
                                            </View>
                                        })                               
                                    : null
                                }
                                <TouchableOpacity style={styles.placephotostyle} onPress={() => {this.onImageUpload(1); this.forceUpdate();}}>
                                    <Image source={require('../images/icon-create-group.png')} style={{width: 50, height: 50, margin: 'auto'}}/>
                                </TouchableOpacity>
                            </ScrollView>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        )
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

}

const offset = 16;

const shadow ={
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:8,
  shadowOffset:{width:0,height:4}
};

const styles = StyleSheet.create({
    placephotostyle: {
        backgroundColor: '#CED5DF', 
        borderRadius: 5, 
        justifyContent: 'center', 
        alignItems: 'center', 
        width: 130, 
        height: 160
    },  
    blockstyle: {
        marginTop: 10,
        borderRadius: 5,
        paddingTop: 10,
        paddingBottom: 10
    },
    title: {
        marginTop: offset,
        marginLeft: offset,
        fontSize: offset
    },
    nameInput: {
        height: offset * 2,
        margin: offset,
        paddingHorizontal: offset,
        borderColor: '#111111',
        borderWidth: 1,
        fontSize: offset
    },
    btn: {
        width: '60%',
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: '#005ca4'
    },
    btnContainer_left: {
        zIndex: 100,
        marginTop: offset,
        flexDirection: 'row',
        padding: 15,
        width: "50%",
        justifyContent: "flex-end",
        alignItems: 'flex-end'
    },
    btnContainer_right: {
        zIndex: 100,
        marginTop: offset,
        flexDirection: 'row',
        padding: 15,
        width: "50%",
        justifyContent: "flex-start",
        alignItems: 'flex-start'
    },
    imgBackground: {
        width: deviceWidth,
        height: '25%',
        flex: 1,
        alignItems: 'center'
    },
    startScanBtn: {
        width: deviceWidth * 0.27,
        height: 35,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 25,
        alignItems: 'center',
    },
    buttonText: {
        marginTop: 8,
        fontSize: 17,
        //fontFamily: 'Gill Sans',
        textAlign: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        backgroundColor: 'transparent',
    },
    progressBar: {
        backgroundColor: 'rgb(3, 154, 229)',
        height: 3,
        shadowColor: '#000',
    },
    chatList:{
        padding: 10,
        marginLeft:5,
        marginRight:5,
        width:deviceWidth-40,
        marginBottom:40
    },
    chatUserBox:{
        width:deviceWidth-60,
        height:100,
        borderRadius:10,
        backgroundColor:'#ffffff',
        justifyContent:'center',
        marginTop:10,
        marginBottom:10,
        marginLeft:5,
        padding:10
    },
    btnbadge:{
        width:30,
        height:30,
        justifyContent:'center',
        padding:5,
        borderRadius:15,
        marginTop:5,
    },
    chatTime:{
        color:'#005ca4',
        fontSize:11,
    
    },
    chatName:{
        color:'#282829',
        fontSize:18,
    },
    chatText:{
        color:'#005ca4',
        fontSize: 18,
    },
    contentText: {
        color: "grey",
        fontSize: 18
    },
    buttonStyle:{
        alignItems:'center',
        padding:10,
        height:60
    },
    selectedButtonStyle:{
        borderRadius:5,
        alignItems:'center',
        padding:10,
        height:60,
        backgroundColor:'#82ceec'
    },
    mainDisplay:{
        alignItems:'center',
        backgroundColor:'#ffffff',
        borderRadius:10,
        marginTop:10,
        width:deviceWidth-30,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    headerTitle:{
        marginTop:-30,
        // marginRight:deviceWidth-360,
        color:'#ffffff',
        textAlign:'center',
        alignItems: 'center',
        width:200,
    },
    headerTitleText:{
        color:'#ffffff',
        fontSize: 25
    },
});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#000000',
        borderRadius: 4,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: '#000000',
        borderRadius: 8,
        color: '#000000',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputBox: {
        width: '100%',
        height: 45,
        backgroundColor: '#1c2635',
        paddingHorizontal: 20,
        fontSize:16,
        color: '#ffffff',
        marginVertical: 10,
        //textAlign: 'center'
    },    
});
  