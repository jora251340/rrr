import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';
import User from '../User';
import firebaseSDK from '../firebaseSDK';
import { Use } from 'react-native-svg';
import NewsNote from '../NewsNote';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }
  componentWillMount(){
    firebaseSDK;
  }
  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    User.email= await AsyncStorage.getItem('userEmail');
    User.name = await AsyncStorage.getItem('userName');
    User.avatar = await AsyncStorage.getItem('userAvatar');
    // alert("name = " + User.name + ", email = " + User.email);
    
    // let unReadNews = JSON.parse(await AsyncStorage.getItem('news'));
    // if (!unReadNews) {
    //   NewsNote.isExist = false;
    //   NewsNote.news = [];
    // } else {
    //   NewsNote.isExist = true;
    //   NewsNote.news = unReadNews;
    // }
    
    this.props.navigation.navigate((User.email && User.name && User.avatar)? 'Explore' : 'Auth')
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}