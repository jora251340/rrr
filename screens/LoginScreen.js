import React, { Component } from 'react';
import { AsyncStorage, TouchableOpacity, Alert, Text, View,Image,StatusBar,ImageBackground } from 'react-native';
import { YellowBox } from 'react-native';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import base64 from 'react-native-base64';
import firebase from 'firebase';

import User from '../User';
import styles from '../constants/styles';
import firebaseSDK from '../firebaseSDK';

// ignore Yellow warnings from Firebase.
YellowBox.ignoreWarnings(['Setting a timer', '']);

export default class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    name: '',
    email: '',
    avatar: '',
    password: ''
  }

  // function for changing state value
  handleChange = key => val => {
    this.setState({ [key]: val })
  }

  componentWillMount() {
  }

  // submit function for login
  submitForm = async () => {
    const user = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      avatar: this.state.avatar
    };
    // validate
    if (this.state.email.length === 0) {
      Alert.alert('Error', ' Email cannot be empty')
    } else if (this.state.password.length === 0) {
      Alert.alert('Error', ' Password cannot be empty')
    } else if (this.state.email.length < 5) {
      Alert.alert('Error', 'wrong Email')
    } else if (this.state.password.length < 3) {
      Alert.alert('Error', 'Wrong Password')
    } else {
      const response = firebaseSDK.login(
        user,
        this.loginSuccess,
        this.loginFailed
      );

      User.email = this.state.email;
      let useremail = base64.encode(User.email);

      // get user infos from Firebase
      let dbRef2 =  firebase.database().ref('/users/' + useremail)
      dbRef2.once('value', (val) => {
        let user = val.val();
        if (user != null) {
          var username = user.name;
          var useravatar = user.avatar;
          User.name = username;
          name = username;
          this.setState ({
            email: User.email,
            avatar: useravatar,
            name: username,
          })
        }
      });
      // store user infos in AsyncStorage
      await AsyncStorage.setItem('userEmail', this.state.email);
      await AsyncStorage.setItem('userName', this.state.name);
      await AsyncStorage.setItem('userAvatar', this.state.avatar);
    }
  }

  loginSuccess = () => {
    this.props.navigation.navigate('Explore', {
      name: this.state.name,
      email: this.state.email,
      avatar: this.state.avatar,
    });

    User.email = this.state.email;
    User.name = this.state.name;
    User.avatar = this.state.avatar;

    AsyncStorage.setItem('userEmail', this.state.email);
    AsyncStorage.setItem('userName', this.state.name);
    AsyncStorage.setItem('userAvatar', this.state.avatar);

    let encEmail = base64.encode(User.email, 'user object email');
    const newemail = User.email;
    this.props.navigation.navigate('Explore', { UserEmailnew: newemail });
  };

  loginFailed = () => {
    alert('Login failure. Please tried again.');
  };

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#005497" barStyle="light-content"/>
        <ImageBackground style={styles.imgBackground } 
                  resizeMode='cover' 
                  source={require('../images/header2.png')}>
          <Content>
            <Form style={{alignItems:'center'}}>
              <Image source={require('../images/logo.png')} style={{width:150,height:150,marginTop:150}} />
              <Item fixedLabel>
                <Label style={{color:'#0b315e'}}>Email</Label>
                <Input
                  placeHolder="test@live.com"
                  value={this.state.email}
                  onChangeText={this.handleChange('email')}
                />
              </Item>
              <Item fixedLabel last>
                <Label style={{color:'#0b315e'}} >Password</Label>
                <Input 
                  value={this.state.password}
                  onChangeText={this.handleChange('password')}
                />
              </Item>
              <View style={styles.btnContainer}>
                <TouchableOpacity  onPress={this.submitForm}>
                  <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                    locations={[0,0.6]}
                    colors={['#005da5', '#007ddf']} style={styles.startScanBtn}>
                    <Text style={styles.buttonText}>
                      Login
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
              <View style={styles.btnContainer}>
                <TouchableOpacity   onPress={() => this.props.navigation.navigate('Signup')}>
                  <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                                  locations={[0,0.6]}
                                  colors={['#005da5', '#007ddf']} style={styles.startScanBtn}>
                    <Text style={styles.buttonText}>
                      Sign up
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </Form>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}

