import React, { Component } from 'react';
import { ScrollView, FlatList, Image, AsyncStorage, TouchableOpacity,Platform, Text, View, StyleSheet, Dimensions, ImageBackground, StatusBar } from 'react-native';
import { ListItem, Left, Body, Right, Thumbnail, Spinner, Segment, Button } from 'native-base';
import firebase from 'firebase';
import base64 from 'react-native-base64';
import {BoxShadow} from 'react-native-shadow';
import LinearGradient from 'react-native-linear-gradient';

import User from '../User';

//const emailnew = ''
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const shadowOpt = {
	width:deviceWidth-50,
  color:"#000",
  height:85,
	border:1,
	radius:5,
	opacity:0.2,
	x:0,
	y:2,
	style:{marginVertical:3,alignItems:'center',backgroundColor:'#ffffff'}
}
export default class ChatCity extends Component {  
  static navigationOptions =({navigation}) => {
    return {
        header:null,      
    }   
  }
  constructor(props) {
    super(props);
    this.state = {
      countryname:props.navigation.getParam('countryname'),
      cityname:props.navigation.getParam('name'),
      loading:true,
      loadingpublic:true,
      loadingprivate:true,
      users: [],
      activePage:1,
      groups: [],
      publicgroups: [],   
      persons: [],
      current: firebase.database.ServerValue.TIMESTAMP,
      timer: null,
    }
  }
  
  selectComponent = (activePage) => () => this.setState({activePage})
  getInitialState() {
    return({
        state: {
        countryname:props.navigation.getParam('countryname'),
        cityname:props.navigation.getParam('name'),
          loading:true,
          loadingpublic:true,
          loadingprivate:true,
          users: [],
          groups: [],
          publicgroups: [],
          persons: [],
          current: firebase.database.ServerValue.TIMESTAMP,
          timer: null,
        }
    })
  }

  componentDidMount() {
    // let timer = setInterval(this.getNewMessages, 5000);
    // this.setState({timer: timer});
  }

  componentWillUnmount() {
    this.clearInterval(this.state.timer);
  }

  componentWillMount() {    
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email:val })
        console.log('email in storage',this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email=noQuotes;
        console.log('new user email in func',User.email)
      }
    })
    
    // get persons chatting
    if (!this.state.cityname) {
      let cityname;
      let dbRef2 =  firebase.database().ref('/users/' + base64.encode(User.email))
      dbRef2.once('value', (val) => {
        let user = val.val();
        if (user != null) {
          cityname = user.cityname
        }
      });
      let dbRef = firebase.database().ref('users');
      dbRef.on('child_added', (val) => {
        let person = val.val();
        person.email = val.key;
        let decEmail = base64.decode(person.email);
        let selectedcityname = cityname;
        if (decEmail === User.email) {
          User.name = person.name
        } else {
          if(person.cityname === selectedcityname){
            this.setState((prevState) => {
              return {
                loading:false,
                persons: [...prevState.persons, person]
              }
            })
          } 
        }
      })
    } else {
      let dbRef4 = firebase.database().ref('messages').child(base64.encode(User.email));
      dbRef4.on('child_added', (val) => {
        let emails = [];
        let objects = val.val();
        if (objects != null) {
          Object.keys(objects).map(value => {
            if (objects[value].from != User.email) {
                var found = emails.find(function(element) {
                return element == objects[value].from;
              });
              if (found == undefined)
                emails.push(objects[value].from);
            }
          });

          Object.keys(emails).map(value => {
            let dbRef5 = firebase.database().ref('users/' + base64.encode(emails[value]));
            dbRef5.on('value', (val) => {
              this.setState((prevState) => {
                return {
                  persons: [...prevState.persons, val.val()]
                }
              })
            })
          })
          // alert(JSON.stringify(this.state.persons));
        }
        this.setState({loading: false})
      })  
    }      

    let dbRef2 = firebase.database().ref('Groups');
    dbRef2.on('child_added', (val) => {
      let Group = val.val();
      Group.id = val.key;
      console.log('chat group id',Group.id);
      let encmemberemail=base64.encode(User.email);
      let dbRef2 = firebase.database().ref('Groups/'+ Group.id +'/GroupMembers/'+encmemberemail);
      dbRef2.once('value', (val) => {
        let person = val.val();
        if(person!= null){
          person.email = val.key;
          let selectedcityname =this.state.cityname;
          let GroupType = "Private";
          if(Group.Grouptype===GroupType&&Group.cityname===selectedcityname){
            // alert(person.email);
            this.setState((prevState) => {
              return {
                loadingprivate:false,
                groups: [...prevState.groups, Group]
              }
            })
          } 
        } 
      })
    })
    let dbRef3 = firebase.database().ref('Groups');
    dbRef3.on('child_added', (val) => {
      let PublicGroup = val.val();
  
      PublicGroup.id = val.key;
      let selectedcityname = this.state.cityname;
      let GroupType = "Public";
      //console.log('chat group id',Group.id);
      let encmemberemail=base64.encode(User.email);
              
      if(PublicGroup.Grouptype===GroupType&&PublicGroup.cityname===selectedcityname){
          //alert(PublicGroup.id)
        this.setState((prevState) => {
          return {
              loadingpublic:false,
            publicgroups: [...prevState.publicgroups, PublicGroup]
          }
        }) 
      }
    })  
  }

  renderRow = ({ item }) => {
    return (    
      <TouchableOpacity style={[styles.chatUserBox, shadow]} onPress={() => this.props.navigation.navigate('Chat', item)}>
        <ListItem avatar  onPress={() => this.props.navigation.navigate('Chat', item)}>
          <Left>
            <Thumbnail source={{ uri: item.avatar }} />
          </Left>
          <Body style={{borderBottomColor: '#ffffff'}} >
            <Text style={styles.chatName}>{item.name}</Text>
            <Text  style={styles.chatText} note>Doing what you like. .</Text>
          </Body>
          <Right style={{borderBottomColor: '#ffffff'}}>
            <Text style={styles.chatTime} note>3:43 PM</Text>
            <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.6}}
                            locations={[0,0.6]}
                            colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
              <Text style={{color: '#ffffff'}}>02</Text>
            </LinearGradient>      
          </Right>
        </ListItem>
      </TouchableOpacity>     
    )
  }
  renderRowGroup = ({ item }) => {
    return (    
      <TouchableOpacity style={[styles.chatUserBox, shadow]} onPress={() => this.props.navigation.navigate('GroupChat', item)}>
        <ListItem avatar  onPress={() => this.props.navigation.navigate('GroupChat', item)}>
          <Left>
            <Thumbnail source={{ uri: item.avatar }} />
          </Left>
          <Body style={{borderBottomColor: '#ffffff'}} >
            <Text style={styles.chatName}>{item.groupname}</Text>
            <Text  style={styles.chatText} note>Lorem ipsum Sit amet</Text>
          </Body>
          <Right style={{borderBottomColor: '#ffffff'}}>
            <Text style={styles.chatTime} note>3:43 PM</Text>
            <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                            locations={[0,0.6]}
                            colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
              <Text style={{color: '#ffffff'}}>02</Text>
            </LinearGradient>      
          </Right>
        </ListItem>
      </TouchableOpacity>
    )
  }
  renderRowPublicGroup = ({ item }) => {
    return (    
      <TouchableOpacity style={[styles.chatUserBox, shadow]} onPress={() => this.props.navigation.navigate('GroupChat', item)}>
        <ListItem avatar  onPress={() => this.props.navigation.navigate('GroupChat', item)}>
          <Left>
            <Thumbnail source={{ uri: item.avatar }} />
          </Left>
          <Body style={{borderBottomColor: '#ffffff'}} >
            <Text style={styles.chatName}>{item.groupname}</Text>
            <Text  style={styles.chatText} note>{item.GroupDescription}</Text>
          </Body>
          <Right style={{borderBottomColor: '#ffffff'}}>
            <Text style={styles.chatTime} note>3:43 PM</Text>
            <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                            locations={[0,0.6]}
                            colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
              <Text style={{color: '#ffffff'}}>02</Text>
            </LinearGradient>         
          </Right>
        </ListItem>
      </TouchableOpacity>
    )
  }
  renderRowPrivateGroup = ({ item }) => {
    return (    
      <TouchableOpacity style={[styles.chatUserBox, shadow]} onPress={() => this.props.navigation.navigate('GroupChat', item)}>
        <ListItem avatar  onPress={() => this.props.navigation.navigate('GroupChat', item)}>
          <Left>
            <Thumbnail source={{ uri: item.avatar }} />
          </Left>
          <Body style={{borderBottomColor: '#ffffff'}} >
            <Text style={styles.chatName}>{item.groupname}</Text>
            <Text  style={styles.chatText} note>{item.GroupDescription}</Text>
          </Body>
          <Right style={{borderBottomColor: '#ffffff'}}>
            <Text style={styles.chatTime} note>3:43 PM</Text>
            <LinearGradient start={{x: 0.5, y: 0.25}} end={{x: 0.5, y: 1.8}}
                            locations={[0,0.6]}
                            colors={['#005da5', '#007ddf']} style={styles.btnbadge}>
              <Text style={{color: '#ffffff'}}>02</Text>
            </LinearGradient>         
          </Right>
        </ListItem>
      </TouchableOpacity>
    )
  }
  // _renderTitle = () => {
  //   if(this.state.activePage === 1)
  //     return (
  //         <Text style={styles.headerTitleText}>Find People</Text>
  //     ) 
  //   else if(this.state.activePage === 2)
  //     return (
  //         <Text style={styles.headerTitleText}>Public Groups</Text>
  //     ) 
  //   else if(this.state.activePage === 3)
  //     return (
  //         <Text style={styles.headerTitleText}>My Groups</Text>
  //     )
  // }
  _renderComponent = () => {
    if(this.state.activePage === 1)
      if (this.state.loading) {
        return (            
          <View style={{ marginTop: 10, alignItems: 'center', justifyContent:'center'}}>
            <Text>Loading Users...</Text>
            <Spinner />
            <FlatList style={styles.chatList} />
          </View>
        );
      } else {
        return (
          <View>
            <ScrollView>
              <FlatList
                data={this.state.persons}
                renderItem={this.renderRow}
                keyExtractor={(item) => item.from}
                style={styles.chatList}
              />
            </ScrollView>
          </View>
        );
      }
     //... Your Component 1 to display
    else if(this.state.activePage === 2)
      if (this.state.loadingpublic) {
        return (        
          <View style={{ marginTop: 10, alignItems: 'center', justifyContent:'center'}}>
            <Text>Loading Group Chats...</Text>
            <Spinner />         
            <FlatList style={styles.chatList} />
            {
              this.state.publicgroups.length > 0 ? <Text></Text> : <Text>No public groups available in this city</Text>
            }
          </View>
        );
      } else {
        return (
          <View>
            <FlatList
              data={this.state.publicgroups}
              renderItem={this.renderRowPublicGroup}
              keyExtractor={(item) => item.id}
              style={styles.chatList}
            />       
          </View>
        );
      }
    else if(this.state.activePage === 3)
      if (this.state.loadingprivate) {
        return (    
            <View style={{ marginTop: 10, justifyContent: "center", alignItems: "center"}}>
              <Text>Loading Group Chats...</Text>
              <Spinner />
              <FlatList style={styles.chatList_Group} />
              {
                this.state.groups.length > 0 ?<Text></Text>:<Text>No Private groups available in this city</Text>
              }
            </View>
        );
      } else {
        return (
            <View>
              <FlatList
                data={this.state.groups}
                renderItem={this.renderRowGroup}
                keyExtractor={(item) => item.id}
                style={styles.chatList_Group}
              />            
            </View>
        );
      }
    else if(this.state.activePage === 4)
      if (this.state.loadingpublic) {
        return (    
            <View style={{ marginTop: 10, justifyContent: "center", alignItems: "center"}}>
              <Text>Loading Group Chats...</Text>
              <Spinner />
              <FlatList style={styles.chatList_Group} />
              {
                this.state.groups.length > 0 ?<Text></Text>:<Text>No Private groups available in this city</Text>
              }
            </View>
        );
      } else {
        return (
            <View>
              <FlatList
                data={this.state.groups}
                renderItem={this.renderRowPublicGroup}
                keyExtractor={(item) => item.id}
                style={styles.chatList_Group}
              />            
            </View>
        );
      }
    else if(this.state.activePage === 5)
      if (this.state.loadingprivate) {
        return (    
          <View style={{ marginTop: 10, justifyContent: "center", alignItems: "center"}}>
            <Text>Loading Group Chats...</Text>
            <Spinner />
            <FlatList style={styles.chatList_Group} />
            {
              this.state.groups.length > 0 ?<Text></Text>:<Text>No Private groups available in this city</Text>
            }
          </View>
        );
      } else {
        return (
          <View>
            <FlatList
              data={this.state.groups}
              renderItem={this.renderRowGroup}
              keyExtractor={(item) => item.id}
              style={styles.chatList_Group}
            />    
          </View>
        );
      }
    else if(this.state.activePage === 6)
      if (this.state.loadingprivate) {
        return (    
          <View style={{ marginTop: 10, justifyContent: "center", alignItems: "center"}}>
            <Text>Loading Group Chats...</Text>
            <Spinner />
            <FlatList style={styles.chatList_Group} />
            {
              this.state.groups.length > 0 ?<Text></Text>:<Text>No Private groups available in this city</Text>
            }
          </View>
        );
      } else {
        return (
          <View>
            <FlatList
              data={this.state.groups}
              renderItem={this.renderRowPrivateGroup}
              keyExtractor={(item) => item.id}
              style={styles.chatList_Group}
            />   
          </View>
        );
      }
  }
  render() {
    return(
      <ImageBackground style={styles.imgBackground} 
                      resizeMode='cover' 
                      source={require('../images/header1.png')}>
        <TouchableOpacity style={{marginTop: 15, marginLeft: deviceWidth-70}} onPress={ () => this.props.navigation.toggleDrawer() }>
          <Image source={require('../images/menu-new.png')} style={{width: 20, height: 20, marginTop: 10, marginRight: 10}} />
        </TouchableOpacity>
        <TouchableOpacity style={{marginTop:-28, marginRight: deviceWidth-70}}>
          <Image source={require('../images/bell-new.png')} style={{width:20, height:25, marginTop: 5, marginLeft: 10}} />
        </TouchableOpacity>  
        <StatusBar backgroundColor="#005497" barStyle="light-content"/>
        {/* <View style={[styles.mainDisplay, shadow]}>  */}
        <View style={{alignItems:'center', fontSize: 20, marginBottom: 5}}>
          <Image source={require('../images/logo.png')} style={{width: 120, height: 120, marginTop: -30}} />
          <Segment  style={{backgroundColor: '#ffffff', height: 40, justifyContent:'space-around',borderColor: Platform.OS === 'ios'? '#fafafa':'#fbfbfb'}}>
            <Button first active={this.state.activePage === 1}  style={(this.state.activePage === 1)? styles.selectedButtonStyle:styles.buttonStyle} onPress={this.selectComponent(1)}  >
              {/* <Image  source={require('../images/peoples.png')} style={styles.tabIconStyle} /> */}
              <Text style={{fontSize: 20}}>Recent</Text>
            </Button>
            {/* <Button active={this.state.activePage === 2 }  onPress={this.selectComponent(2)}  style={(this.state.activePage === 2)? styles.selectedButtonStyle:styles.buttonStyle} > */}
            <Button style={styles.buttonStyle} >
              {/* <Image  source={require('../images/publicg.png')} style={styles.tabIconStyle} /> */}
              <Text style={{fontSize: 20}}>Chat</Text>
            </Button>
            <Button  active={this.state.activePage === 3}  onPress={this.selectComponent(3)}  style={(this.state.activePage === 3)? styles.selectedButtonStyle:styles.buttonStyle} >
              {/* <Image  source={require('../images/privateg.png')} style={styles.tabIconStyle} /> */}
              <Text style={{fontSize: 20}} style={{fontSize: 20}}>Groups</Text>
            </Button>
          </Segment>
        </View>
        <ScrollView>
          {this._renderComponent()}
        </ScrollView>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateGroup')} style={styles.addGroupempty}>
          <Image  source={require('../images/icon-chat-plus.png')}  style={{width:30,height:30}}/>        
        </TouchableOpacity>
        { this.state.activePage > 2 ? 
          <View style={{alignItems:'center', position: 'absolute', bottom: 0}}>
            <Segment  style={{borderColor: Platform.OS === 'ios'? '#fafafa':'#fbfbfb',backgroundColor: '#ffffff', height: 60, justifyContent: 'space-around', fontSize: 20, width: deviceWidth-20, marginLeft: 5, marginRight: 5}}>
              <Button first active={this.state.activePage === 3}  style={(this.state.activePage === 3)? styles.selectedTabBtnStyle:styles.tabStyle} onPress={this.selectComponent(3)}  >
                <Image  source={require('../images/icons/icon-all-bottom.png')} style={styles.tabIconStyle} />
                <Text  style={styles.bottomtab_text}>All</Text>
              </Button>
              <Button active={this.state.activePage === 4 }  onPress={this.selectComponent(4)}  style={(this.state.activePage === 4)? styles.selectedTabBtnStyle:styles.tabStyle} >
                <Image  source={require('../images/icons/icon-public-bottom.png')} style={styles.tabIconStyle} />
                <Text  style={styles.bottomtab_text}>Public</Text>
              </Button>
              <Button  active={this.state.activePage === 5}  onPress={this.selectComponent(5)}  style={(this.state.activePage === 5)? styles.selectedTabBtnStyle:styles.tabStyle} >
                <Image  source={require('../images/icons/icon-private-bottom.png')} style={styles.tabIconStyle} />
                <Text style={styles.bottomtab_text}>Private</Text>
              </Button>
              <Button  active={this.state.activePage === 6}  onPress={this.selectComponent(6)}  style={(this.state.activePage === 6)? styles.selectedTabBtnStyle:styles.tabStyle} >
                <Image  source={require('../images/icons/icon-users-bottom.png')} style={styles.tabIconStyle} />
                <Text style={styles.bottomtab_text}>My Groups</Text>
              </Button>
            </Segment>
          </View>
        :
          <View></View>
        }
      </ImageBackground>
    )   
  }  
}

const offset = 16;
const shadow ={
  shadowColor:'#005ca4',
  shadowRadius:10,
  shadowOpacity:0.4,
  elevation:8,
  shadowOffset:{width:0,height:4}
}
const styles = StyleSheet.create({
    title: {
        marginTop: offset,
        marginLeft: offset,
        fontSize: offset
    },
    nameInput: {
        height: offset * 2,
        margin: offset,
        paddingHorizontal: offset,
        borderColor: '#111111',
        borderWidth: 1,
        fontSize: offset
    },
    buttonText: {
        marginLeft: offset,
        fontSize: 42
    },
    btn:{
      width:deviceWidth-20,
      justifyContent:'center'
    },
    btnContainer:{
      marginTop:offset,
      alignItems:'center',
      justifyContent:'center',
      flexDirection:'row',
      padding:10,
    },
    imgBackground: {
      width: deviceWidth,
      height: '15%',
      flex: 1,
      alignItems:'center'
  },
  startScanBtn: {
    width:deviceWidth-20,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 25,
    alignItems:'center',
  },
  buttonText: {
    fontSize: 16,
    //fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontWeight:'bold',
    backgroundColor: 'transparent',
  },
  chatList:{
    marginLeft:5,
    marginRight:5,
    width:deviceWidth-40,
    height:deviceHeight-150,
    marginTop:10,
  },
  chatList_Group:{
    marginLeft:5,
    marginRight:5,
    width:deviceWidth-40,
    height:deviceHeight-300,
    marginTop:10,
  },
  chatUserBox:{
    width: deviceWidth-50,
    height: 80,
    borderRadius: 10,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5,
    padding: 10
  },
btnbadge:{
  width:30,
  height:30,
  justifyContent:'center',
  padding:5,
  borderRadius:15,
  marginTop:5,
},
chatTime:{
  color:'#005ca4',
  fontSize:11,

},
chatName:{
  color:'#282829',
  fontSize:16,
},
chatText:{
  color:'#9e9e9e',
  fontSize:11,
},
buttonStyle:{
  backgroundColor:'#fafafa',
    borderColor:'white',
    padding:10,
    height:60,
    width: '30%',
    justifyContent:'center',
    textAlign: 'center'
  },
  selectedButtonStyle:{
    backgroundColor: 'white',
    padding: 10,
    height: 60,
    borderColor: 'white',
    borderBottomColor: '#006ec4',
    borderWidth: 3,
    fontWeight: '300',
    width: '30%',
    textAlign: 'center',
    justifyContent:'center'
  },
  tabStyle: {
    backgroundColor:Platform.OS ==='ios'?'#fafafa':'white',
    padding:10,
    height:60,
    width: '25%',
    borderColor: 'white',
    justifyContent:'center',
    textAlign: 'center',
    flexDirection: 'column'
  },
  selectedTabBtnStyle: {
    backgroundColor: 'white', 
    padding: 10,
    height: 60,
    fontWeight: '300',
    width: '25%',
    borderColor: 'white',
    textAlign: 'center',
    justifyContent:'center',
    flexDirection: 'column'
  },  
  tabIconStyle: {
    width: 25,
    height: 25
  },
  mainDisplay:{
    alignItems:'center',
    backgroundColor:'#ffffff',
    borderRadius:10,
    marginTop:20,
    width:deviceWidth-30,
  },
  headerTitle:{
    marginTop:-28,
    marginRight:deviceWidth-360,
    color:'#ffffff',
    textAlign:'center',
    alignItems:'center',
    width:200,
  },
  headerTitleText:{
    color:'#ffffff',
    fontSize:16,
    textAlign:'center',
    fontWeight:'bold'
  },
  addGroup:{
    position:'absolute',
    zIndex:11,
    right:20,
    bottom:220,
    width:60,
    height:60,
    alignItems:'center',
    justifyContent:'center',
    elevation:8,
  },
  addGroupempty:{
    position:'absolute',
    zIndex:11,
    right:20,
    bottom:60,
    width:60,
    height:60,
    alignItems:'center',
    justifyContent:'center',
    elevation:8,
  },
  bottomtab_text:{
    fontSize: Platform.OS === 'ios' ? 13:14
  }
});
