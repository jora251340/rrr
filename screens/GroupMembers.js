import React, { Component } from 'react';
import { Platform, SafeAreaView, FlatList,Image,AsyncStorage, TouchableOpacity, Alert, TextInput,  View } from 'react-native';
import { Container, Header, Content, Text, Button, Toast ,ListItem, Left, Body, Right, Thumbnail,SwipeRow,Icon,List} from "native-base";
import User from '../User';
import styles from '../constants/styles';
import firebase from 'firebase';
import base64 from 'react-native-base64';
//const emailnew = ''
let membermail;
let memberavatar;
let membername;

export default class GroupMembers extends Component {
  static navigationOptions =({navigation}) => {
    return {
      title: 'Group Members',
      headerRight:(
        <TouchableOpacity onPress={()=>navigation.navigate('Profile')}>
          <Image source={require('../images/user.png')} style={{width:32,height:32,marginRight:7}}/>
        </TouchableOpacity>
      ),      
    }   
  }
  constructor(props) {
    super(props);
    this.state = {
        users: [],
        groupid:props.navigation.getParam('GroupId'),
        showToast: false
    }
  this.addMember = this.addMember.bind(this);
}

addMember(membermail,membername,memberavatar) {
    //let decmembermail = base64.decode(membermail)
   //console.log('GroupCreator',GroupCreator);
   let GroupId = this.state.groupid;
   let decmembermail = base64.decode(membermail);
  
   let dbRef = firebase.database().ref('Groups/'+ GroupId +'/GroupMembers/'+membermail);
    dbRef.once('value', (val) => {
     let person = val.val();
     
     // console.log('User passed email',User.email)
   // console.log('User predefiend',User.name)
   // console.log(User.email);
   if(person!= null){
    person.email = val.key;
     // alert(person.email);
     let decEmail = base64.decode(person.email);
    
       alert('Member Alreaady Exits in the group');
       
     
     
     
     
   }else{
    GroupId = this.state.groupid;
    const Groupmember = {
        membername: membername,
        memberemail:membermail,
        memberavatar:memberavatar
    };
  //  await AsyncStorage.setItem('userEmail', this.state.email);
   // User.email = this.state.email;
   // firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
    // firebase.createAccount(user);
    firebase.database().ref('Groups/' + GroupId + '/GroupMembers/'+membermail).set({ membername: Groupmember.membername, memberemail:base64.decode(Groupmember.memberemail), memberavatar:Groupmember.memberavatar })
         alert('Member Added Successfully')
    }
    })
  }
  deleteRow( rowId) {
    const newData = [...this.state.users];
    newData.splice(rowId, 1);
    this.setState({ users: newData });
  }

  componentWillMount() {
    AsyncStorage.getItem('userEmail').then(val => {
      if (val) {
        this.setState({ email:val,}
        )
        console.log('email in storage',this.state.email);
        //return val;
        var noQuotes = this.state.email.split('"').join('');
        User.email = noQuotes;
        console.log('new user email in func',User.email)
      }
    })

//console.log('email from storage',val);
//User.email=this.state.email;
//console.log('new user email out func',User.email)
//console.log('user name stored in stat',User.email);
//  console.log('user email got from route',UserEmailtest);
    let dbRef = firebase.database().ref('Groups/'+this.state.groupid+'/GroupMembers/');
    dbRef.on('child_added', (val) => {
     let person = val.val();
     // console.log('User passed email',User.email)
   // console.log('User predefiend',User.name)
   // console.log(User.email);
      person.email = val.key;
      let decEmail = base64.decode(person.email);
    console.log(decEmail);
     // console.log('email in db selection',User.email);
        this.setState((prevState) => {
          return {
            users: [...prevState.users, person]
          }
        })
      

    })
  }

  renderRow = ({ item }) => {
    return (
      
       <SwipeRow
       rightOpenValue={-75}
       body={
         <View>
             <Left>
               <Thumbnail source={{ uri:item.memberavatar  }} />
            </Left>
               <Body>
           <Text>{item.membername}</Text>
            </Body>
         </View>
       }
       right={
         <Button danger onPress={() =>this.deleteRow(item.memberemail)}>
           <Icon active name="trash" />
         </Button>
       }
     />
    )
  }
  render() {
    return (
      <SafeAreaView>
        <FlatList
          data={this.state.users}
          renderItem={this.renderRow}
          keyExtractor={(item) => item.memberemail}
        />
         
        
      </SafeAreaView>
    );
  }
  
}

