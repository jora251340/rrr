import firebase from 'firebase';
import RNFetchBlob from 'rn-fetch-blob'
import uuid from 'react-native-uuid';
import {
  Platform
} from 'react-native';
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class FirebaseSDK {
  constructor() {
    if (!firebase.apps.length) {
      //avoid re-initializing
      firebase.initializeApp({
        apiKey: "AIzaSyBSEd9230H9QuZw31yfxytelQ0eym_P5Z4",
        authDomain: "twmtheapp-9cead.firebaseapp.com",
        databaseURL: "https://twmtheapp-9cead.firebaseio.com",
        projectId: "twmtheapp-9cead",
        storageBucket: "twmtheapp-9cead.appspot.com",
        messagingSenderId: "397476397753",
        appId: "1:397476397753:web:bc399f3374652be2"
      // firebase.initializeApp({
      //   apiKey: "AIzaSyCL8pldLwhaAkR047zLxuW1IydpvP8eHe4",
      //   authDomain: "twmtheapp-9cead.firebaseapp.com",
      //   databaseURL: "https://twmtheapp-9cead.firebaseio.com",
      //   projectId: "twmtheapp-9cead",
      //   storageBucket: "twmtheapp-9cead.appspot.com",
      //   messagingSenderId: "397476397753",
      //   appId: "1:397476397753:android:c61cc839daef7074"
      });
    }
  }
  
  login = async (user, success_callback, failed_callback) => {
    await firebase
      .auth()
      .signInWithEmailAndPassword(user.email, user.password)
      .then(success_callback, failed_callback);
  };
   uploadImage = (uri, imageName, mime = 'image/jpg') => {
    
    return new Promise((resolve, reject) => {
    //  const image = currentImage.uri
    //  const uploadUri = Platform.OS === 'android' ? uri.replace('content://', '') : uri
    const ext = uri.split('.').pop(); // Extract image extension
    const filename = `${uuid()}.${ext}`; // Generate unique name
    const uploadUri =uri
      console.log(uploadUri);
       let uploadBlob = null
       const imageRef = firebase.storage().ref('avatars').child(filename)
       let mime = 'image/jpg'
      //  const imageRef = firebase.storage().ref('avatars').child(imageName).put(uploadUri, { contentType: mime })
        fs.readFile(uploadUri, 'base64')
      .then((data) => {
        return Blob.build(data, { type: `${mime};BASE64` })
    })
    .then((blob) => {
        uploadBlob = blob
        return imageRef.put(blob, { contentType: mime })
      })
      .then(() => {
        uploadBlob.close()
        return imageRef.getDownloadURL()
      })
      .then((url) => {
        // URL of the image uploaded on Firebase storage
        console.log(url);
        
      })
      .catch((error) => {
        console.log(error);

      })
    })
  }
 
 /* uploadImage = async uri => {
    console.log('got image to upload. uri:' + uri);
    try {
      const response = await fetch(uri);
      const blob = await response.blob();
      const ref = firebase
        .storage()
        .ref('avatar')
        .child(uuid.v4());
      const task = ref.put(blob);

      return new Promise((resolve, reject) => {
        task.on(
          'state_changed',
          () => {
            /* noop but you can track the progress here */
         /* },
          reject /* this is where you would put an error callback! */
         /* () => resolve(task.snapshot.downloadURL)
        );
      });
    } catch (err) {
      console.log('uploadImage try/catch error: ' + err.message); //Cannot load an empty url
    }
  }*/



  onLogout = user => {
    firebase.auth().signOut().then(function () {
      console.log("Sign-out successful.");
    }).catch(function (error) {
      console.log("An error happened when signing out");
    });
  }

  get uid() {
    return (firebase.auth().currentUser || {}).uid;
  }

  get ref() {
    return firebase.database().ref('Messages');
  }

  parse = snapshot => {
    const { timestamp: numberStamp, text, user } = snapshot.val();
    const { key: id } = snapshot;
    const { key: _id } = snapshot; //needed for giftedchat
    const timestamp = new Date(numberStamp);

    const message = {
      id,
      _id,
      timestamp,
      text,
      user,
    };
    return message;
  };

  refOn = callback => {
    this.ref
      .limitToLast(20)
      .on('child_added', snapshot => callback(this.parse(snapshot)));
  }

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  // send the message to the Backend
  send = messages => {
    for (let i = 0; i < messages.length; i++) {
      const { text, user } = messages[i];
      const message = {
        text,
        user,
        createdAt: this.timestamp,
      };
      this.ref.push(message);
    }
  };

  refOff() {
    this.ref.off();
  }
}



const firebaseSDK = new FirebaseSDK();



export default firebaseSDK;